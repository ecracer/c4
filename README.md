# C4 #

A static serializability checker for convergent causal consistency. C4
is designed to be independent of the data store, data store API, and
programming language, and thus to serve as a basis for the analysis of
any kind of system that satisfies convergence, atomic visibility and
causal consistency. C4 can therefore be used as a back
end for a broad range of static analyses: in the context of distributed
systems and databases it can be applied to weakly consistent mobile
synchronization frameworks like TouchDevelop, to distributed databases
like Antidote, Walter, CORS, and Eiger, and even to traditional
relational databases with guarantees weaker than serializability but at
least as strong as our model, e.g., Snapshot Isolation. Furthermore, it
is useful as a bug-checker for clients of databases with weaker
guarantees, for example Cassandra, Dynamo or Riak.

C4 is interfaced by static analysis front ends, which are 
responsible for inferring a sound abstract history from application
source code and providing a precise algebraic specification. We have
implemented two front ends based on standard static analysis techniques,
for TouchDevelop and Cassandra. The corresponding abstract histories
are included in this repository.

## License ##

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, you can obtain one at http://mozilla.org/MPL/2.0/.

## How to compile ##

Install `sbt` to compile. http://www.scala-sbt.org/

Install `z3` >=4.5 to run. https://github.com/Z3Prover/z3/releases

Type `sbt compile` to compile the code.

Type `sbt test` to test the code.

Type `sbt assembly` to produce a runnable JAR.

Type `sbt run` to run the tool.

The tool takes a JSON document from `stdin`. For an example of the
format, see [this example](src/test/resources/example/putputgetget.json).

## Produce benchmark table ##

To produce the table from the paper, run `stats.sh` (this requires
running `sbt assembly` first. It contains extra column `Unclassified`
which should be 0 and is omitted in the paper, `Harmless` and `Warning`
which are summed to `Harmless` in the paper, and `TimeAnalysis` and
`TimeGeneralizes`, which are summed to `Time` in the paper.

## Producing venn diagrams ##

After running `sbt assembly`, one can produce the Venn diagrams from
the paper for the included benchmarks. For this purpose, run `feature_venn.sh`
or `filter_venn.sh`, which will produce 4 files, which can be turned into
a venn diagram using the online tool http://bioinformatics.psb.ugent.be/webtools/Venn/ .

## Classifications ##

The classification for the violations are contained in
`src/test/resources/*/*.txt`. Each line contains a unique identifier
of a violation and a `=h` at the end of the line for a harmless violation,
a `=e` for an error, a `=w` for a warning, `=n` for a false alarm. In
the paper, we do not distinguish between warnings and harmless violations.

## Number of sessions ##

This implementation of C4 only finds violations with up to 2 sessions; that
is, it implements the algorithms described in the paper for the special
case k=2. The output of the tool is a set of violations, and whether this
result generalizes to an arbitrary number of sessions.

