#!/usr/bin/env bash

declare -a touchdevelop=("blqz" "cvuz" "eddm" "eijba" "etww" "fqaba" "gcane" "kjxzcgcv" "kmac" "nggfa" "nvoha" "padg" "qwidc" "qzju" "ruef" "ulvma" "wbuei")
declare -a cassandra=("cassandra-lock" "cassandra-twitter" "cassatwitter" "cassieq-core" "currency-exchange" "datastax-queueing" "killrchat" "playlist" "roomstore" "shopping-cart" "twissandra")

CFOUR="java -jar cfour.jar"
INPUT_DIRECTORY=src/test/resources

for i in "${touchdevelop[@]}"
do
    echo ======= ${i}
	${CFOUR} -println -unfold -touchdevelopFiltered < ${INPUT_DIRECTORY}/touchdevelop/${i}.model.json
done
for i in "${cassandra[@]}"
do
    echo ======= ${i}
	${CFOUR} -println -unfold -cassandraFiltered  < ${INPUT_DIRECTORY}/cassandra/${i}-filtered.json
done
