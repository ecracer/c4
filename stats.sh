#!/usr/bin/env bash

declare -a touchdevelop=("blqz" "cvuz" "eddm" "eijba" "etww" "fqaba" "gcane" "kjxzcgcv" "kmac" "nggfa" "nvoha" "padg" "qwidc" "qzju" "ruef" "ulvma" "wbuei")
declare -a cassandra=("cassandra-lock" "cassandra-twitter" "cassatwitter" "cassieq-core" "currency-exchange" "datastax-queueing" "killrchat" "playlist" "roomstore" "shopping-cart" "twissandra")

CFOUR="java -jar cfour.jar"
INPUT_DIRECTORY=src/test/resources

echo "#### Unfiltered"
echo -e "ID\tTransactions\tEvents\tTotal\tError\tWarning\tHarmless\tFalseAlarm\tUnclassified\tTimeAnalysis\tGeneralizes?\tTimeGeneralization"

for i in "${touchdevelop[@]}"
do
    echo -n ${i}
    echo -ne '\t'
	${CFOUR} -stats -touchdevelopUnfiltered \
	    -classificationFile=${INPUT_DIRECTORY}/touchdevelop/${i}.txt <  ${INPUT_DIRECTORY}/touchdevelop/${i}.model.json
done
for i in "${cassandra[@]}"
do
    echo -n ${i}
    echo -ne '\t'
	${CFOUR} -stats -cassandraUnfiltered \
	    -classificationFile=${INPUT_DIRECTORY}/cassandra/${i}-violations.txt <  ${INPUT_DIRECTORY}/cassandra/${i}-unfiltered.json
done

echo "#### Filtered"
echo -e "ID\tTransactions\tEvents\tTotal\tError\tWarning\tHarmless\tFalseAlarm\tUnclassified\tTimeAnalysis\tGeneralizes?\tTimeGeneralization"

for i in "${touchdevelop[@]}"
do
    echo -n ${i}
    echo -ne '\t'
	${CFOUR} -stats -touchdevelopFiltered \
	    -classificationFile=${INPUT_DIRECTORY}/touchdevelop/${i}.txt <  ${INPUT_DIRECTORY}/touchdevelop/${i}.model.json
done
for i in "${cassandra[@]}"
do
    echo -n ${i}
    echo -ne '\t'
	${CFOUR} -stats -cassandraFiltered \
	    -classificationFile=${INPUT_DIRECTORY}/cassandra/${i}-violations.txt  <  ${INPUT_DIRECTORY}/cassandra/${i}-filtered.json
done
