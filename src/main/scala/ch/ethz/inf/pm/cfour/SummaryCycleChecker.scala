/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package ch.ethz.inf.pm.cfour

import ch.ethz.inf.pm.cfour.AbstractHistory.TransactionID
import ch.ethz.inf.pm.cfour.output.LabeledGraph

object SummaryCycleChecker {

  def makeGraph(g: AbstractHistory)(implicit param: Parameters): LabeledGraph[String, String] = {
    implicit val graph: AbstractHistory = g
    val lg = g.toLabeledGraph
    for (t1 <- g.transactions; t2 <- g.transactions) {
      if (antiDep(t1, t2)) {
        lg.addEdge(TransactionNames.make(t1), TransactionNames.make(t2), Some("ad"))
      }
      if (dep(t1, t2)) {
        lg.addEdge(TransactionNames.make(t1), TransactionNames.make(t2), Some("de"))
      }
      if (arb(t1, t2)) {
        lg.addEdge(TransactionNames.make(t1), TransactionNames.make(t2), Some("cf"))
      }
    }
    lg
  }

  private def nonAbsorbing(e: Event, f: Event)(implicit g: AbstractHistory, param: Parameters): Boolean = {
    g.possiblyNotAbsorbedBySMT(e, f)
  }

  private def nonCommutative(e: Event, f: Event)(implicit g: AbstractHistory, param: Parameters): Boolean = {
    g.possiblyNonCommutativeSMT(e, f)
  }

  def antiDep(t1: TransactionID, t2: TransactionID)(implicit g: AbstractHistory, param: Parameters): Boolean = {
    for (q <- g.queriesByTxn.getOrElse(t1, Set.empty); u <- g.updatesByTxn.getOrElse(t2, Set.empty) if nonCommutative(q, u))
      return true
    false
  }

  def dep(t1: TransactionID, t2: TransactionID)(implicit g: AbstractHistory, param: Parameters): Boolean = {
    for (u <- g.updatesByTxn.getOrElse(t1, Set.empty); q <- g.queriesByTxn.getOrElse(t2, Set.empty) if nonCommutative(u, q))
      return true
    false
  }

  def arb(t1: TransactionID, t2: TransactionID)(implicit g: AbstractHistory, param: Parameters): Boolean = {
    for (u1 <- g.updatesByTxn.getOrElse(t1, Set.empty); u2 <- g.updatesByTxn.getOrElse(t2, Set.empty) if nonCommutative(u1, u2))
      return true
    false
  }

  def dsg(t1: TransactionID, t2: TransactionID)(implicit g: AbstractHistory, param: Parameters): Boolean = {
    antiDep(t1, t2) || arb(t1, t2) || dep(t1, t2)
  }

  def mayHaveViolations(g: AbstractHistory, t1: TransactionID, t2: TransactionID)(implicit param: Parameters): Boolean = {
    implicit val graph: AbstractHistory = g
    ((arb(t1, t2) && antiDep(t2, t1)) ||
      (antiDep(t1, t2) && arb(t2, t1)) ||
      (antiDep(t1, t2) && antiDep(t2, t1))
      ) &&
      extraConditionFulfilled(Set(t1, t2))
  }

  def mayHaveViolations(g: AbstractHistory, t1: ToEdge, t2: TransactionID)(implicit param: Parameters): Boolean = {
    implicit val graph: AbstractHistory = g
    (!t1.isSynchronized &&
      (
        (arb(t1.targetID, t2) && antiDep(t2, t1.sourceID)) ||
          (antiDep(t1.targetID, t2) && arb(t2, t1.sourceID)) ||
          (antiDep(t1.targetID, t2) && antiDep(t2, t1.sourceID))
        )
      ) &&
      extraConditionFulfilled(Set(t1.sourceID, t1.targetID, t2))
  }

  def mayHaveViolations(g: AbstractHistory, t1: ToEdge, t2: ToEdge)(implicit param: Parameters): Boolean = {
    implicit val graph: AbstractHistory = g
    ((arb(t1.targetID, t2.sourceID) && antiDep(t2.targetID, t1.sourceID) && !t2.isSynchronized) ||
      (antiDep(t1.targetID, t2.sourceID) && arb(t2.targetID, t1.sourceID) && !t1.isSynchronized) ||
      (antiDep(t1.targetID, t2.sourceID) && antiDep(t2.targetID, t1.sourceID) && !(t1.isSynchronized || t2.isSynchronized))) &&
      extraConditionFulfilled(Set(t1.sourceID, t1.targetID, t2.sourceID, t2.targetID))
  }

  def mayHaveViolations(t1: (TransactionID, TransactionID), t2: TransactionID)
    (implicit g: AbstractHistory, param: Parameters): Boolean = {
    ((arb(t1._2, t2) && antiDep(t2, t1._1)) ||
      (antiDep(t1._2, t2) && arb(t2, t1._1)) ||
      (antiDep(t1._2, t2) && antiDep(t2, t1._1))) && extraConditionFulfilled(Set(t1._1, t1._2, t2))
  }

  def mayHaveViolations(t1: (TransactionID, TransactionID), t2: (TransactionID, TransactionID))
    (implicit g: AbstractHistory, param: Parameters): Boolean = {
    ((arb(t1._2, t2._1) && antiDep(t2._2, t1._1)) ||
      (antiDep(t1._2, t2._1) && arb(t2._2, t1._1)) ||
      (antiDep(t1._2, t2._1) && antiDep(t2._2, t1._1))) && extraConditionFulfilled(Set(t1._1, t1._2, t2._1, t2._2))
  }

  def extraConditionFulfilled(ts: Set[TransactionID])
    (implicit g: AbstractHistory, param: Parameters): Boolean = {

    if (param.useExtraCondition) {
      for (
        t1 <- ts; u1 <- g.updatesByTxn.getOrElse(t1, Set.empty);
        t2 <- ts; u2 <- g.updatesByTxn.getOrElse(t2, Set.empty)
      ) {
        if (nonAbsorbing(u1, u2)) return true
      }

      for (
        t <- ts; q <- g.queriesByTxn.getOrElse(t, Set.empty); u <- g.transitiveProgramOrderView(t).successors(q.id)
      ) {
        val uE = g.eventMap(u)
        if (uE.operationID != Skip.id && uE.operation.isUpdate)
          return true
      }

      false
    } else {
      true
    }
  }

}
