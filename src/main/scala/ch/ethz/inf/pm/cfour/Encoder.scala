/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package ch.ethz.inf.pm.cfour

import ch.ethz.inf.pm.cfour.AbstractHistory._
import ch.ethz.inf.pm.cfour.ExprBuilder._
import ch.ethz.inf.pm.cfour.output.ClassificationMap.Unclassified
import ch.ethz.inf.pm.cfour.output._
import ch.ethz.inf.pm.cfour.utils.SimpleGraph

import scala.util.Random

object Encoder extends Logger {

  def findViolations(g: AbstractHistory, problemName: Option[String] = None)(implicit param: Parameters): Option[LabeledGraph[String, String]] = {

    assert(g.transactionOrderView.isAcyclic)
    assert(g.transactions.forall(g.programOrderView(_).isAcyclic))

    val session = problemName.getOrElse(Random.alphanumeric.take(8).mkString(""))

    Statistics.numberSMTQueries += 1
    if (param.debug) {
      log_debug("Dumping input graph to " + DumpGraph.dumpToFile(session + "_input", g.toLabeledGraph))
    }

    var violation: Option[LabeledGraph[String, String]] = None
    val conv = EventGraphZ3Converter(param)

    Z3Prover.withZ3[Unit, Expr, Var]({ z3 =>

      serializabilityChecking(g, z3, param)
      z3.check() match {
        case Z3Prover.Sat =>
          val model = z3.extractModel()
          logger.debug(model.mkString("\n"))
          val cycle = toLabeledGraph(model, conv)(g, param)
          val named =
            problemName.flatMap { name: String =>
              param.classificationMap.map { map =>
                val classification = map.getOrElse(name, Unclassified)
                Statistics.classificationMap(classification) = Statistics.classificationMap.getOrElse(classification, Set.empty) + name
                "Violation(" + classification + ")"
              }
            }.getOrElse("Violation")
          if (param.dumpViolationIDs) println(named + " " + session)
          if (param.noExport) log_info(s"$named found for combination " + session)
          else log_info(s"$named found, exported to " + DumpGraph.dumpToFile(session + "_cycle", cycle))
          violation = Some(cycle)
        case Z3Prover.Unknown =>
          log_info("ERROR: Could not solve")
        case Z3Prover.Unsat =>
          if (param.noExport) log_info("Violation ruled out for combination " + session)
          else log_info("Violation ruled out: " + DumpGraph.dumpToFile(session + "_nocycle", SummaryCycleChecker.makeGraph(g)))
      }

      if (param.debug) {
        log_debug("Problem size: " + z3.bookkeeper.assumptionSize)
        log_debug("encode(Dumping) log to " + FileSystemExporter.export(session + "_smt_log", z3.bookkeeper.smtLog.reverse.mkString("\n")))
      }

    }, conv)

    violation
  }

  def toLabeledGraph(model: Map[String, String], conv: EventGraphZ3Converter)(implicit g: AbstractHistory, param: Parameters): LabeledGraph[String, String] = {

    val pg = g.toLabeledGraph

    pg.filterEdges { (x: String, y: String, _: Option[String]) => TransactionNames.isName(x) && TransactionNames.isName(y) }
    var nodeLabels = Map.empty[String, Map[String, String]]

    var ar = SimpleGraph[String]()
    var ca = SimpleGraph[String]()

    val ActiveVar = """active-(.*)""".r

    def E(x: String) = conv.unshortEventName(x)

    def T(x: String) = TransactionNames.make(conv.unshortTxnName(x))

    model.foreach {
      case (edgeString, "true") if EdgeNames.isName(edgeString) =>
        val (label, src, tgt) = EdgeNames.deconstruct(edgeString)
        if (label == "dconf" || label == "dplus" || label == "dminus") {
          pg.addEdge(T(src), T(tgt), Some(label))
          pg.setEdgeClass((T(src), T(tgt), Some(label)), "violation")
        }
        if (label == "po") {
          pg.addEdge(E(src), E(tgt), Some(label))
          pg.setEdgeClass((E(src), E(tgt), Some(label)), "path")
        }
        if (label == "ar") {
          ar += (T(src), T(tgt))
        }
        if (label == "ca") {
          ca += (T(src), T(tgt))
        }
      case (edgeString, "false") if EdgeNames.isName(edgeString) =>
        val (label, src, tgt) = EdgeNames.deconstruct(edgeString)
        if (label == "po") {
          pg.addEdge(E(src), E(tgt), Some(label))
          pg.setEdgeClass((E(src), E(tgt), Some(label)), "inactive")
        }
      case (nodeString, right) if EventArgumentNames.isName(nodeString) =>
        val (node, sort, n) = EventArgumentNames.deconstruct(nodeString)
        nodeLabels = nodeLabels + (E(node) -> (nodeLabels.getOrElse(E(node), Map.empty) + (sort + n -> right)))
      case (ActiveVar(n), "false") =>
        pg.setNodeClass(E(n), "inactive")
      case (ActiveVar(n), "true") =>
        pg.setNodeClass(E(n), "event")
      case (someVar, someValue) =>
        pg.addExtraInformation(s"$someVar = $someValue")
    }


    var to = SimpleGraph[String]()
    for ((a, b) <- g.transactionOrderView.edgeList) {
      to += (TransactionNames.make(a), TransactionNames.make(b))
    }

    ar = ar.transitiveReduction -- to
    for (a <- ar.edgeSet) {
      pg.addEdge(a._1, a._2, Some("ar"))
      pg.setEdgeClass((a._1, a._2, Some("ar")), "ar")
    }

    ca = ca.transitiveReduction -- to
    for (a <- ca.edgeSet) {
      pg.addEdge(a._1, a._2, Some("ca"))
      pg.setEdgeClass((a._1, a._2, Some("ca")), "ca")
    }

    for (a <- to.edgeSet) {
      pg.setEdgeClass((a._1, a._2, Some("to")), "violation")
    }

    nodeLabels.foreach(x =>
      if (pg.nodes.contains(x._1))
        pg.setNodeLabel(x._1, x._1 + ":"
          + g.eventMap(x._1).operationID + " ("
          + x._2.toList.sorted.map(y => y._1 + "=" + y._2).mkString(",") + ")")
    )

    pg
  }

  def serializabilityChecking(implicit g: AbstractHistory, ass: Assume[Expr], param: Parameters): Unit = {

    import ExprBuilder._

    axiomatizeExecution
    axiomatizeDependencyGraph

    ::("==== Serializability")

    ::("Transitivity of TD")
    for (u <- g.transactions; v <- g.transactions)
      assume(TDSG(u, v) iff
        bigOr(
          (if (u != v) List(DSG(u, v)) else Nil)
            :::
            (for (w <- g.transactions if u != w && v != w) yield {
              DSG(u, w) and TDSG(w, v)
            })
        )
      )

    ::("Reflexivity of TD")
    assume(bigOr(for (t <- g.transactions) yield TDSG(t, t)))

  }

  def inNamedAtomicSet(e: Event)(implicit g: AbstractHistory, param: Parameters): Expr = {
    bigOr(for ((name, preds) <- g.system.namedAtomicSets.toList) yield {
      preds.list.find(_.operations.contains(e.operationID)) match {
        case Some(pred) => NamedAtomicSet(name) and pred.e.instantiate(e)
        case None => False
      }
    })
  }

  def hasAntiDependency(e: TransactionID, f: TransactionID)(implicit g: AbstractHistory, param: Parameters): Expr = {
    bigOr(
      for (q <- g.queriesByTxn.getOrElse(e, Nil); u <- g.updatesByTxn.getOrElse(f, Nil)) yield
        bigAnd(
          Active(u.id) ::
            Active(q.id) ::
            (if (param.encodeCommutativity) not(q commutesWith u) else if ((q commutesWith u) != True) True else False) ::
            (if (param.encodeAtomicSet) InAtomicSet(u.id) and InAtomicSet(q.id) else True) ::
            (if (param.encodeNamedAtomicSets) inNamedAtomicSet(u) and inNamedAtomicSet(q) else True) ::
            not(Vi(u.txn, q.txn)) ::
            (if (param.encodeAbsorption) {
              for (v <- g.updates) yield {
                val uArbV =
                  if (u.txn == v.txn && g.transitiveProgramOrderView(u.txn).hasEdge(u.id, v.id)) True
                  else if (u.txn == v.txn) False
                  else Ar(u.txn, v.txn)
                val vVisQ =
                  if (v.txn == q.txn && g.transitiveProgramOrderView(v.txn).hasEdge(v.id, q.id)) True
                  else if (v.txn == q.txn) False
                  else Vi(v.txn, q.txn)
                not(bigAnd(
                  Active(v.id) ::
                    uArbV ::
                    vVisQ ::
                    List(u.absorbedBy(v, Some(q)))
                ))
              }
            } else Nil)
        )
    )
  }

  def hasDependency(e: TransactionID, f: TransactionID)(implicit g: AbstractHistory, param: Parameters): Expr = {
    bigOr(for (u <- g.updatesByTxn.getOrElse(e, Nil); q <- g.queriesByTxn.getOrElse(f, Nil)) yield
      bigAnd(
        Active(u.id) ::
          Active(q.id) ::
          (if (param.encodeCommutativity) not(u commutesWith q) else if ((u commutesWith q) != True) True else False) ::
          (if (param.encodeAtomicSet) InAtomicSet(u.id) and InAtomicSet(q.id) else True) ::
          (if (param.encodeNamedAtomicSets) inNamedAtomicSet(u) and inNamedAtomicSet(q) else True) ::
          Vi(u.txn, q.txn) ::
          (if (param.encodeAbsorption) {
            for (v <- g.updates) yield {
              val uArbV =
                if (u.txn == v.txn && g.transitiveProgramOrderView(u.txn).hasEdge(u.id, v.id)) True
                else if (u.txn == v.txn) False
                else Ar(u.txn, v.txn)
              val vVisQ =
                if (v.txn == q.txn && g.transitiveProgramOrderView(v.txn).hasEdge(v.id, q.id)) True
                else if (v.txn == q.txn) False
                else Vi(v.txn, q.txn)
              not(bigAnd(
                Active(v.id) ::
                  uArbV ::
                  vVisQ ::
                  List(u.absorbedBy(v, Some(q)))
              ))
            }
          } else Nil)
      )
    )
  }

  def hasConflictDependency(e: TransactionID, f: TransactionID)(implicit g: AbstractHistory, param: Parameters): Expr = {
    // Update Conflicts
    bigOr(for (u <- g.updatesByTxn.getOrElse(e, Nil); v <- g.updatesByTxn.getOrElse(f, Nil)) yield
      bigAnd(
        Active(u.id) ::
          Active(v.id) ::
          Ar(u.txn, v.txn) ::
          (if (param.encodeCommutativity) not(u commutesWith v) else if ((u commutesWith v) != True) True else False) ::
          (if (param.encodeAtomicSet) InAtomicSet(u.id) and InAtomicSet(v.id) else True) ::
          (if (param.encodeNamedAtomicSets) inNamedAtomicSet(u) and inNamedAtomicSet(v) else True) ::
          Nil
      )
    )
  }

  def encodeDSG(e: TransactionID, f: TransactionID)(implicit g: AbstractHistory, ass: Assume[Expr]): Unit = {
    assume(Iff(
      DSG(e, f),
      BigOr(
        (if (g.transactionOrder.exists(x => x.sourceID == e && x.targetID == f)) List(True) else Nil)
          ::: List(
          DConf(e, f),
          DMinus(e, f),
          DPlus(e, f)
        )
      )
    ))
  }

  def encodeAxiomaticModel()(implicit g: AbstractHistory, ass: Assume[Expr], param: Parameters): Unit = {

    ::("To in Ca")
    for (e <- g.transactionOrder)
      assume(Vi(e.sourceID, e.targetID))

    ::("Ca implies Ar")
    for (u <- g.transactions; v <- g.transactions if u != v)
      assume(Vi(u, v) implies Ar(u, v))

    ::("Transitivity of Ca")
    for (u <- g.transactions; v <- g.transactions; w <- g.transactions if w != u && w != v)
      assume((Vi(u, w) and Vi(w, v)) implies Vi(u, v))

    ::("Irreflexivity of Ca")
    for (u <- g.transactions)
      assume(not(Vi(u, u)))

    ::("Totality of Ar")
    for (u <- g.transactions; v <- g.transactions if u != v)
      assume(Ar(u, v) or Ar(v, u))

    ::("Transitivity of Ar")
    for (u <- g.transactions; v <- g.transactions; w <- g.transactions if w != u && w != v)
      assume((Ar(u, w) and Ar(w, v)) implies Ar(u, v))

    ::("Irreflexivity of Ar")
    for (u <- g.transactions)
      assume(not(Ar(u, u)))

    ::("Prefix consistency")
    if (param.encodePrefixConsistency) {
      for (u <- g.transactions; v <- g.transactions if !g.isTransactionOrdered(u, v); w <- g.transactions if w != u && w != v)
        assume((Vi(u, v) and Ar(w, u)) implies Vi(w, v))

      for (ToEdge(src, tgt, true) <- g.transactionOrder;
           u <- g.transactions if tgt != u && src != u) {
        assume(Ar(u, src) implies Vi(u, tgt))
      }
    }

  }

  def encodeProcess(t: TransactionID)(implicit g: AbstractHistory, ass: Assume[Expr], param: Parameters): Unit = {

    ::("Source and sinks active")
    assume(Active(g.source(t)) and Active(g.sink(t)))

    for (p <- g.programOrderByTransaction.getOrElse(t, Set.empty)) {
      assume(Po(p.sourceID, p.targetID) implies (Active(p.sourceID) and Active(p.targetID)))
    }

    ::("Non-sinks are active exactly if they are followed by 1 active event")
    for (e <- g.eventsByTxn(t) if e.id != g.sink(t)) {
      assume(Active(e.id) iff
        oneOf(
          for (e2 <- g.programOrderBySource(e.id)) yield {
            Po(e.id, e2.targetID)
          }
        )
      )
    }

    ::("Non-sources are active exactly if they are preceded by 1 active event")
    for (e <- g.eventsByTxn(t) if e.id != g.source(t)) {
      assume(Active(e.id) iff
        oneOf(
          for (e2 <- g.programOrderByTarget(e.id)) yield {
            Po(e2.sourceID, e.id)
          }
        )
      )
    }

    if (param.encodeConstraints) {
      ::("Constraints on program order")
      for (x <- g.programOrderByTransaction.getOrElse(t, Set.empty)) {
        assume(Po(x.sourceID, x.targetID) implies x.constraint.instantiate(x.source, x.target))
      }
    }

    ::("Building transitive process")
    for (u <- g.eventsByTxn(t); v <- g.eventsByTxn(t))
      assume(TPo(u.id, v.id) iff
        bigOr(
          (if (g.isProgramOrdered(u, v)) List(Po(u.id, v.id)) else Nil)
            :::
            (for (w <- g.eventsByTxn(t) if g.isTransitivelyProgramOrdered(t, u, v) && g.isProgramOrdered(u, w) && v.id != w.id) yield {
              Po(u.id, w.id) and TPo(w.id, v.id)
            })
        )
      )
  }

  def axiomatizeExecution(implicit g: AbstractHistory, ass: Assume[Expr], param: Parameters): Unit = {

    if (param.encodeConstraints) {
      ::("==== Global/event constraints")
      assume(g.globalConstraint)
      var uniques: Set[Var] = Set.empty
      var uniqueCounter = 0
      for (e <- g.events)
        assume(e.constraint.instantiate(e).replace {
          case FreshUniqueVar(s) =>
            val v = AuxVar(s, "unique_" + uniqueCounter)
            uniques += v
            uniqueCounter = uniqueCounter + 1
            v
          case UniqueTransactionLocalVar(id, txn, s) =>
            val v = TransactionLocalVar(id, txn, s)
            uniques += v
            v
          case x => x
        })
      for (u1 <- uniques; u2 <- uniques if u1 != u2) {
        assume(u1 unequal u2)
      }
      if (param.encodeNamedAtomicSets) {
        ::("==== One atomic set")
        assume(oneOf(g.system.namedAtomicSets.keys.map(NamedAtomicSet).toList))
      }
    }

    if (param.encodeProcesses) {
      ::("==== Processes")
      for (t <- g.transactions) {
        encodeProcess(t)
      }
    }

    if (param.encodeLegalitySpec) {
      ::("===== Legality Spec")
      for (a <- g.transactions; b <- g.transactions) {
        if (a == b) {
          if (param.encodeProcesses) {
            for (e1 <- g.eventsByTxn(a); e2 <- g.eventsByTxn(a)) {
              g.system.legalitySpec.get(e1.operationID + "," + e2.operationID).foreach { x =>
                assume(TPo(e1.id, e2.id) implies x.instantiate(e1, e2))
              }
            }
          }
        } else {
          for (e1 <- g.eventsByTxn(a); e2 <- g.eventsByTxn(b)) {
            g.system.legalitySpec.get(e1.operationID + "," + e2.operationID).foreach { x =>
              assume(Vi(a, b) implies x.instantiate(e1, e2))
            }
          }
        }
      }
    }

    if (param.encodeImpliedVisibility) {
      ::("===== Implied Visibility")
      for (a <- g.transactions; b <- g.transactions) {
        if (a == b) {
          if (param.encodeProcesses) {
            for (e1 <- g.eventsByTxn(a); e2 <- g.eventsByTxn(a)) {
              g.system.impliedVisibility.get(e1.operationID + "," + e2.operationID).foreach { x =>
                assume(x.instantiate(e1, e2) implies TPo(e1.id, e2.id))
              }
            }
          }
        } else {
          for (e1 <- g.eventsByTxn(a); e2 <- g.eventsByTxn(b)) {
            g.system.impliedVisibility.get(e1.operationID + "," + e2.operationID).foreach { x =>
              assume(x.instantiate(e1, e2) implies Vi(a, b))
            }
          }
        }
      }
    }

    if (param.encodeSynchronizingOperations) {
      ::("===== Synchronization Spec")
      for (a <- g.transactions; b <- g.transactions if a != b) {
        for (e1 <- g.eventsByTxn(a); e2 <- g.eventsByTxn(b)) {
          g.system.synchronizationSpec.get(e1.operationID + "," + e2.operationID).foreach { x =>
            assume(x.instantiate(e1, e2) implies Vi(a, b))
          }
        }
      }
    }

    ::("==== Axiomatic Model")
    encodeAxiomaticModel()

  }

  def axiomatizeDependencyGraph(implicit g: AbstractHistory, ass: Assume[Expr], param: Parameters): Unit = {

    if (param.encodeAtomicSet) {
      if (g.system.atomicSets.nonEmpty) {

        ::("=== Assume one partition is active")
        assume(oneOf(for ((_, i) <- g.system.atomicSets.zipWithIndex) yield {
          AtomicSet(i)
        }))

        for ((p, i) <- g.system.atomicSets.zipWithIndex) {

          val paramMap = p.freeVars.map(x => (x, AtomicSetParameter(x.count, i, x.sort))).toMap

          for ((op, constraint) <- p.operations; e <- g.eventsByOp.getOrElse(op, Set.empty)) {
            assume(
              (InAtomicSet(e.id) and AtomicSet(i))
                implies
                constraint.instantiate(e).replace {
                  case x: FreeVar => paramMap(x)
                  case x: Any => x
                }
            )
          }

        }
      }
    }

    ::("==== Dependency Graph")

    ::("Conflict Dependencies")
    for (e <- g.transactions; f <- g.transactions if e != f)
      assume(Iff(
        DConf(e, f),
        hasConflictDependency(e, f)
      ))

    ::("Dependencies")
    for (e <- g.transactions; f <- g.transactions if e != f)
      assume(Iff(
        DPlus(e, f),
        hasDependency(e, f)
      ))

    ::("Anti-Dependencies")
    for (e <- g.transactions; f <- g.transactions if e != f)
      assume(Iff(
        DMinus(e, f),
        hasAntiDependency(e, f)
      ))

    ::("Dependency Graph")
    for (e <- g.transactions; f <- g.transactions if e != f)
      encodeDSG(e, f)
  }

  def ::(a: String)(implicit ass: Assume[Expr]): Unit = {
    ass.comment(a)
  }

  def assume(a: Expr)(implicit ass: Assume[Expr]): Unit = {
    if (a != True) {
      ass.assume(a)
    }
  }

  trait EdgeVariable extends Var {
    def sort: BoolSort.type = BoolSort

    def n: String

    def l: String

    def r: String
  }

  trait TransactionEdgeVariable extends EdgeVariable

  trait EventEdgeVariable extends EdgeVariable

  case class Active(e: EventID) extends Var {
    def sort: BoolSort.type = BoolSort
  }

  case class Po(l: EventID, r: EventID) extends EventEdgeVariable {
    def n = "po"
  }

  case class TPo(l: EventID, r: EventID) extends EventEdgeVariable {
    def n = "tpo"
  }

  case class Ar(l: TransactionID, r: TransactionID) extends TransactionEdgeVariable {
    def n = "ar"
  }

  case class Vi(l: TransactionID, r: TransactionID) extends TransactionEdgeVariable {
    def n = "ca"
  }

  case class DConf(l: TransactionID, r: TransactionID) extends TransactionEdgeVariable {
    def n = "dconf"
  }

  case class DMinus(l: TransactionID, r: TransactionID) extends TransactionEdgeVariable {
    def n = "dminus"
  }

  case class DPlus(l: TransactionID, r: TransactionID) extends TransactionEdgeVariable {
    def n = "dplus"
  }

  case class DSG(l: TransactionID, r: TransactionID) extends TransactionEdgeVariable {
    def n = "d"
  }

  case class TDSG(l: TransactionID, r: TransactionID) extends TransactionEdgeVariable {
    def n = "td"
  }

  case class NamedAtomicSet(s: String) extends Var {
    override def sort: Sort = BoolSort
  }

  case class AtomicSet(i: Int) extends Var {
    override def sort: Sort = BoolSort
  }

  case class InAtomicSet(e: EventID) extends Var {
    override def sort: Sort = BoolSort
  }

  case class AtomicSetParameter(argNumber: Int, setID: Int, sort: Sort) extends Var

  object EventArgumentNames {

    def make(e: EventID, sort: String, n: Int): String =
      "a-" + e + "-" + sort + "-" + n

    def isName(a: String): Boolean =
      a.startsWith("a-")

    def deconstruct(str: String): (EventID, String, Int) = {
      val Array(a, c, d, e) = str.split("-")
      assert(a == "a")
      (c, d, e.toInt)
    }

  }

  object GlobalVariableNames {

    def make(sort: String, id: String): String =
      "g-" + sort + "-" + id

    def isName(a: String): Boolean =
      a.startsWith("g-")

    def deconstruct(str: String): (String, String) = {
      val Array(a, c, d) = str.split("-")
      assert(a == "g")
      (c, d)
    }

  }

  object ClientLocalVariableNames {

    def make(sort: String, id: String): String =
      "c-" + sort + "-" + id

    def isName(a: String): Boolean =
      a.startsWith("c-")

    def deconstruct(str: String): (String, String) = {
      val Array(a, c, d) = str.split("-")
      assert(a == "c")
      (c, d)
    }

  }

  object EdgeNames {

    def make(label: String, l: TransactionID, r: TransactionID): String =
      "e-" + label + "-" + l + "-" + r

    def isName(a: String): Boolean =
      a.startsWith("e-")

    def deconstruct(str: String): (String, TransactionID, TransactionID) = {
      val Array(a, b, c, d) = str.split("-")
      assert(a == "e")
      (b, c, d)
    }

  }

  def clean(s: String): String = s.replaceAll("\\W+", "_")

}

case class EventGraphZ3Converter(param: Parameters) extends Z3Prover.ExpressionConverter[Expr, Var] {

  import ch.ethz.inf.pm.cfour.Encoder._

  case class AnyVar(s: String) extends Var {
    override def sort: Sort = StringSort
  }

  var eventCounter = 0
  var eventMap = Map.empty[String, Int]

  var txnCounter = 0
  var txnMap = Map.empty[String, Int]

  lazy val txnRevMap: Map[Int, String] = Map() ++ txnMap.map(_.swap)
  lazy val eventRevMap: Map[Int, String] = Map() ++ eventMap.map(_.swap)

  def unshortEventName(e: String): String = {
    if (param.useShortNames) {
      eventRevMap(e.stripPrefix("ev").toInt)
    } else e
  }

  def unshortTxnName(e: String): String = {
    if (param.useShortNames) {
      txnRevMap(e.stripPrefix("txn").toInt)
    } else e
  }

  def shortTxnName(e: EventID): String = {
    if (param.useShortNames) {
      "txn" +
        txnMap.getOrElse(e, {
          txnCounter = txnCounter + 1
          txnMap = txnMap + (e -> txnCounter)
          txnCounter
        })
    } else e
  }

  def shortEventName(e: EventID): String = {
    if (param.useShortNames) {
      "ev" +
        eventMap.getOrElse(e, {
          eventCounter = eventCounter + 1
          eventMap = eventMap + (e -> eventCounter)
          eventCounter
        })
    } else e
  }

  override def name(v: Var): String = v match {
    case ClientLocalVar(id, sort) => ClientLocalVariableNames.make(shortName(sort), id)
    case OtherClientLocalVar(id, sort) => ClientLocalVariableNames.make(shortName(sort), id + "_other")
    case GlobalVar(id, sort) => GlobalVariableNames.make(shortName(sort), id)
    case EventArgVar(n, e, sort) => EventArgumentNames.make(shortEventName(e), shortName(sort), n)
    case ArgLeft(n, sort) => "arg-l-" + n + "-" + shortName(sort)
    case ArgRight(n, sort) => "arg-r-" + n + "-" + shortName(sort)
    case ArgMiddle(n, sort) => "arg-m-" + n + "-" + shortName(sort)
    case x: EventEdgeVariable => EdgeNames.make(x.n, shortEventName(x.l), shortEventName(x.r))
    case x: TransactionEdgeVariable => EdgeNames.make(x.n, shortTxnName(x.l), shortTxnName(x.r))
    case Active(e) => "active-" + shortEventName(e)
    case AtomicSet(i) => "atomic-set-" + i
    case InAtomicSet(e) => "in-atomic-set-" + shortEventName(e)
    case AtomicSetParameter(i, j, sort) => "atomic-set-parameter-" + i + "-" + j + "-" + shortName(sort)
    case NamedAtomicSet(s) => "named-atomic-set-" + s
    case NamedAtomicSetChosenArgument(s1, s2, n, sort) => "named-atomic-set-chosen-argument-" + s1 + "-" + s2 + "-" + n + "-" + shortName(sort)
    case AuxVar(_, name) => "aux_" + name
    case TransactionLocalVar(id, txn, sort) => s"txn-$id-${shortTxnName(txn)}-${shortName(sort)}"
    case FreshUniqueVar(_) => "<unique>"
    case AnyVar(s) => s
  }

  def sorts(expr: Expr): Set[String] = {
    if (!param.useUninterpretedSort) {
      Set.empty
    } else {
      var toDeclare = Set.empty[String]
      expr.replace {
        case v: Var => toDeclare = toDeclare + encode(v.sort); v
        case v: Quantifier => toDeclare = toDeclare ++ v.vs.map(_.sort).map(encode); v
        case x => x
      }
      toDeclare
    }
  }

  override def sort(v: Var): String = {
    encode(v.sort)
  }

  def encode(s: Sort): String = {
    if (!param.useUninterpretedSort)
      s match {
        case BoolSort => "Bool"
        case IntSort => "Int"
        case RealSort => "Real"
        case StringSort => "String"
      }
    else {
      s match {
        case BoolSort => "Bool"
        case IntSort => "Any"
        case RealSort => "Any"
        case StringSort => "Any"
      }
    }
  }

  override def vars(expr: Expr): Set[Var] = {
    if (!param.useUninterpretedSort) {
      expr.freeVars
    } else {
      var toDeclare = Set.empty[Var]
      expr.replace {
        case x@StringConst(str) => toDeclare = toDeclare + AnyVar(clean("str_" + str)); x
        case x@IntConst(i) => toDeclare = toDeclare + AnyVar(clean("int_" + i)); x
        case x@RealConst(d) => toDeclare = toDeclare + AnyVar(clean("real_" + d)); x
        case x => x
      }
      expr.freeVars ++ toDeclare
    }
  }

  override def convert(expr: Expr): String = {
    encode(expr)
  }

  def shortName(sort: Sort): String = {
    encode(sort).head.toLower.toString
  }

  def declare(v: Var): String = {
    "(" + name(v) + " " + encode(v.sort) + ")"
  }

  def encode(expr: Expr): String = {
    expr match {
      case Implies(l, r) => smt2op("=>", encode(l), encode(r))
      case Iff(l, r) => smt2op("=", encode(l), encode(r))
      case And(l, r) => smt2op("and", encode(l), encode(r))
      case Or(l, r) => smt2op("or", encode(l), encode(r))
      case Equal(l, r) => smt2op("=", encode(l), encode(r))
      case GreaterEqual(_, _) if param.useUninterpretedSort => "true"
      case GreaterEqual(l, r) => smt2op(">=", encode(l), encode(r))
      case LessEqual(_, _) if param.useUninterpretedSort => "true"
      case LessEqual(l, r) => smt2op("<=", encode(l), encode(r))
      case Unequal(l, r) => smt2op("not", smt2op("=", encode(l), encode(r)))
      case Exists(vs, e) =>
        smt2op("exists", "(" + vs.map(v => declare(v)).mkString(" ") + ")", encode(e))
      case Forall(vs, e) =>
        smt2op("forall", "(" + vs.map(v => declare(v)).mkString(" ") + ")", encode(e))
      case Not(e) => smt2op("not", encode(e))
      case BigOr(es) => smt2op("or", es.map(encode): _*)
      case BigAnd(es) => smt2op("and", es.map(encode): _*)
      case BigXor(es) => smt2op("xor", es.map(encode): _*)
      case StringConst(str) if param.useUninterpretedSort => Encoder.clean("str_" + str)
      case StringConst(str) => "\"" + str + "\""
      case IntConst(i) if param.useUninterpretedSort => Encoder.clean("int_" + i)
      case IntConst(i) => i.toString
      case RealConst(d) if param.useUninterpretedSort => Encoder.clean("real_" + d)
      case RealConst(d) => d.toString
      case True => "true"
      case False => "false"
      case x: Var => name(x)
    }
  }

  def smt2op(a: String, ops: String*): String = "(" + a + " " + ops.mkString(" ") + ")"
}
