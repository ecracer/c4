/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package ch.ethz.inf.pm.cfour

/** Convenient building of expressions, with simplification for better readability of formulas */
object ExprBuilder {

  implicit def toRichExpr(e: Expr): RichExpr = RichExpr(e)

  case class RichExpr(expr: Expr) {

    def equal(other: Expr): Expr = {
      if (expr == other) True
      else if (expr.hashCode() > other.hashCode()) Equal(expr, other)
      else Equal(other, expr)
    }

    def unequal(other: Expr): Expr = {
      if (expr == other) False
      else if (expr.hashCode() > other.hashCode()) Unequal(expr, other)
      else Unequal(other, expr)
    }

    def <=(other: Expr): Expr = {
      if (expr == other) True
      else LessEqual(expr, other)
    }

    def >=(other: Expr): Expr = {
      if (expr == other) True
      else GreaterEqual(expr, other)
    }

    def and(other: Expr): Expr = {
      if (expr == False || other == False) False
      else if (expr == True) other
      else if (other == True) expr
      else if (expr == other) expr
      else And(expr, other)
    }

    def implies(other: Expr): Expr = {
      if (expr == False) True
      else if (other == True) True
      else if (expr == True) other
      else if (expr == other) True
      else Implies(expr, other)
    }

    def iff(other: Expr): Expr = {
      if (expr == True) other
      else if (expr == False) not(other)
      else if (other == True) expr
      else if (other == False) not(expr)
      else if (expr == other) True
      else Iff(expr, other)
    }

    def or(other: Expr): Expr = {
      if (expr == True || other == True) True
      else if (expr == False) other
      else if (other == False) expr
      else if (expr == other) expr
      else Or(expr, other)
    }

  }

  def bigXor(es: List[Expr]): Expr = {
    es.size match {
      case 0 => False
      case 1 => es.head
      case 2 => Iff(es.head, Not(es.tail.head))
      case _ => BigXor(es)
    }
  }

  def bigAnd(es: List[Expr]): Expr = {
    if (!es.contains(False)) {
      val esFiltered = es.filter(_ != True).distinct
      esFiltered.size match {
        case 0 => True
        case 1 => esFiltered.head
        case 2 => esFiltered.head and esFiltered.tail.head
        case _ => BigAnd(esFiltered)
      }
    } else False
  }

  def bigOr(es: List[Expr]): Expr = {
    if (!es.contains(True)) {
      val esFiltered = es.filter(_ != False).distinct
      esFiltered.size match {
        case 0 => False
        case 1 => esFiltered.head
        case 2 => esFiltered.head or esFiltered.tail.head
        case _ => BigOr(esFiltered)
      }
    } else True
  }

  def bigOrDefaultTrue(es: List[Expr]): Expr = {
    if (!es.contains(True)) {
      val esFiltered = es.filter(_ != False).distinct
      esFiltered.size match {
        case 0 => True
        case 1 => esFiltered.head
        case 2 => esFiltered.head or esFiltered.tail.head
        case _ => BigOr(esFiltered)
      }
    } else True
  }

  def oneOf(es: List[Expr]): Expr = {
    bigOr(es) and
      bigAnd(
        for (e <- es; e2 <- es if e != e2) yield {
          not(e and e2)
        }
      )
  }

  def forall(vs: List[Var], e: Expr): Expr = {
    e match {
      case True => True
      case False => False
      case _ if vs.isEmpty => e
      case _ => Forall(vs, e)
    }
  }

  def exists(vs: List[Var], e: Expr): Expr = {
    e match {
      case True => True
      case False => False
      case _ if vs.isEmpty => e
      case _ => Exists(vs, e)
    }
  }

  def not(e: Expr): Expr = {
    e match {
      case Not(x) => x
      case BigAnd(x) => BigOr(x.map(not))
      case BigOr(x) => BigAnd(x.map(not))
      case Implies(l, r) => And(l, not(r))
      case Exists(vs, es) => Forall(vs, not(es))
      case Forall(vs, es) => Exists(vs, not(es))
      case True => False
      case False => True
      case _ => Not(e)
    }
  }

}
