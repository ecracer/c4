/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package ch.ethz.inf.pm.cfour

import java.io.File

import ch.ethz.inf.pm.cfour.output.ClassificationMap.ClassificationMap

object Parameters {

  def setStats(params: Parameters): Parameters = {
    params.copy(
      outputTSV = true,
      debug = false,
      noExport = true
    )
  }

  def setDebug(params: Parameters): Parameters = {
    params.copy(
      outputTSV = false,
      debug = true,
      noExport = false
    )
  }

  def setCassandraFiltered(params: Parameters): Parameters = {
    params.copy(
      ignoreDisplayCode = true
    )
  }

  def setTouchDevelopFiltered(params: Parameters): Parameters = {
    params.copy(
      encodePrefixConsistency = true,
      ignoreDisplayCode = true,
      encodeNamedAtomicSets = true
    )
  }

  def setCassandraUnfiltered(params: Parameters): Parameters = {
    params.copy(
    )
  }

  def setTouchDevelopUnfiltered(params: Parameters): Parameters = {
    params.copy(
      encodePrefixConsistency = true
    )
  }

  val default = Parameters()

  var get: Parameters = default

  def set(p: Parameters): Parameters = {
    get = p
    p
  }

  lazy val testParameters: Parameters = {

    val param = Parameters.default.copy(debug = false, outputViaPrintln = true)
    param.exportDirectory.foreach { x =>
      val d = new File(x)
      if (!d.exists()) d.mkdir()
    }
    Parameters.set(param)

  }

}

case class Parameters(
    farCommutativityEquivalenceCheck: Boolean = false,
    onlyFarCommutativityEquivalenceCheck: Boolean = false,
    resultGeneralizationCheck: Boolean = true,
    useUninterpretedSort: Boolean = false,
    useStaticSubsetMinimality: Boolean = true,
    encodeConstraints: Boolean = true,
    encodeAbsorption: Boolean = true,
    encodeProcesses: Boolean = true,
    encodeCommutativity: Boolean = true,
    encodeAsymmetricCommutativity: Boolean = true,
    encodeQuerySpecificAbsorption: Boolean = true,
    encodeAtomicSet: Boolean = false,
    encodeLegalitySpec: Boolean = true,
    encodeImpliedVisibility: Boolean = true,
    encodeSynchronizingOperations: Boolean = true,
    encodePrefixConsistency: Boolean = false,
    encodeNamedAtomicSets: Boolean = false,
    outputViaPrintln: Boolean = false,
    outputTSV: Boolean = false,
    debug: Boolean = false,
    unrollOnlyTwice: Boolean = true,
    noExport: Boolean = false,
    ignoreDisplayCode: Boolean = false,
    pointWiseCommutativity: Boolean = false,
    useShortNames: Boolean = true,
    useExtraCondition: Boolean = true,
    checkCommutativityAbsorptionWithSMT: Boolean = true,
    splitStatsBySize: Boolean = false, // Split violation statistics by size
    quiet: Boolean = false,
    dumpViolationIDs: Boolean = false,
    exportDirectory: Option[String] = Some("/tmp/cfour/"),
    classificationMap: Option[ClassificationMap] = None

)
