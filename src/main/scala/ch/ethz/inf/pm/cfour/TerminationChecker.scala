/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package ch.ethz.inf.pm.cfour

import ch.ethz.inf.pm.cfour.AbstractHistory.TransactionID
import ch.ethz.inf.pm.cfour.Encoder.{Ar, Vi}
import ch.ethz.inf.pm.cfour.ExprBuilder._
import ch.ethz.inf.pm.cfour.Unfolder.ViolationSet
import ch.ethz.inf.pm.cfour.Z3Prover.{Sat, Unknown, Unsat}
import ch.ethz.inf.pm.cfour.output.{DumpGraph, FileSystemExporter, Logger}

import scala.collection.mutable.ListBuffer
import scala.util.Random

/**
  *
  * Sanity check:
  * If there is a path C1_A --> C2_B --?--> C2_C ---> C3_D
  * then there is a cycle C1_D --?--> C1_A --> C2_B --?--> C2_C ---> C1_D
  * or a path C3_D --> C2_B --?--> C2_C ---> C3_D --?--> C3_A
  *
  */
object TerminationChecker extends Logger {

  def terminationCheck(graph: AbstractHistory, violations: ViolationSet)(implicit param: Parameters): Boolean = {
    termCheck(graph, violations, param.copy(useUninterpretedSort = true, useStaticSubsetMinimality = true))
  }

  private def termCheck(graph: AbstractHistory, violations: ViolationSet, param: Parameters): Boolean = {
    implicit val p: Parameters = param
    var failed = false
    var checksPerformed = 0
    var checksSucceeded = 0

    for (
      c1trans <- graph.transactions;
      c2trans <- clientTransitions(graph)
      if !violations.hasSubViolation(c2trans, c1trans) &&
        SummaryCycleChecker.dsg(c1trans, c2trans.in)(graph, param)
      ;
      c3trans <- graph.transactions
      if !violations.hasSubViolation(c1trans, c3trans) &&
        !violations.hasSubViolation(c2trans, c3trans) &&
        SummaryCycleChecker.antiDep(c2trans.out, c3trans)(graph, param) &&
        (!param.useStaticSubsetMinimality || !violations.hasSubViolation(Set(c1trans, c3trans) ++ c2trans.toSet))
    ) {
      checksPerformed += 1
      if (!termCheckForFragment(c1trans, c2trans, c3trans, graph)) {

        log_debug(s"Generalization check failed for $c1trans,$c2trans,$c3trans")
        failed = true

      } else {

        checksSucceeded += 1
        log_debug(s"Generalization check succeeded for $c1trans,$c2trans,$c3trans")

      }

    }

    if (param.debug) {
      log_debug(s"Number of generalization checks succeeded: $checksSucceeded", Some(checksSucceeded.toString))
      log_debug(s"Number of generalization checks performed: $checksPerformed", Some(checksPerformed.toString))
    }
    log_info(s"Generalization check: ${!failed}, after $checksPerformed checks", Some(if (failed) "nongen" else "gen"))
    !failed
  }

  def termCheckForFragment(
      c1trans: TransactionID,
      c2trans: ClientTransition,
      c3trans: TransactionID,
      graph: AbstractHistory
  )(implicit param: Parameters): Boolean = {

    def restrictAndUnfold(
        c: ClientTransition
    ): AbstractHistory = {

      c match {
        case SingleClientTransition(x) =>
          graph.restrictToTransaction(x)
        case DoubleClientTransition(x, y) =>
          val z = graph.getTransactionOrder(x, y)
          graph.restrictAndUnfoldTransactionEdge(z.get)

      }

    }

    val p1 = graph.restrictToTransaction(c1trans).instantiateToClient(1, Some(2))
    val p2 = restrictAndUnfold(c2trans).instantiateToClient(2, Some(3))
    val p3 = graph.restrictToTransaction(c3trans).instantiateToClient(3, Some(1))

    val subPath = p1 ++ p2 ++ p3

    val originalPathEdge1 = (p1.txnSink, p2.txnSource)
    val originalPathEdge2 = (p2.txnSink, p3.txnSource)

    if (graph.isTransactionOrdered(c1trans, c2trans.out)) {
      val alt = graph.restrictToTransaction(c2trans.out).renameTransactions("alt_" + _).instantiateToClient(1, Some(2)).removeUniques()
      val shortcut = p1 ++ alt + ToEdge(p1.txnSink, alt.txn)
      val shortCutEdge = (alt.txn, p3.txnSource)
      if (termCheckForSpecShortCut(subPath, shortcut ++ p3 ++ p2, originalPathEdge1, originalPathEdge2, shortCutEdge)) return true
    }

    false
  }

  def termCheckForSpecShortCut(
      originalExecution: AbstractHistory,
      shortcuttingExecution: AbstractHistory,
      originalPathEdge1: (TransactionID, TransactionID),
      originalPathEdge2: (TransactionID, TransactionID),
      shortcut: (TransactionID, TransactionID),
      sessionName: Option[String] = None
  )(implicit param: Parameters): Boolean = {
    val session = sessionName.getOrElse(Random.alphanumeric.take(5).mkString(""))
    debug(originalExecution, shortcuttingExecution, originalPathEdge1, originalPathEdge2, shortcut, session)

    val problem1 = new CollectingAssume
    Encoder.axiomatizeExecution(originalExecution, problem1, param)
    Encoder.axiomatizeDependencyGraph(originalExecution, problem1, param)
    problem1.assume(Encoder.DSG(originalPathEdge1._1, originalPathEdge1._2))
    problem1.assume(Encoder.DMinus(originalPathEdge2._1, originalPathEdge2._2))

    val problem2 = new CollectingAssume
    Encoder.axiomatizeExecution(shortcuttingExecution, problem2, param.copy(encodeImpliedVisibility = false))
    Encoder.axiomatizeDependencyGraph(shortcuttingExecution, problem2, param)

    val implication = (((problem2 -- problem1).toExpr
      and BigAnd(for (t <- originalExecution.transactions) yield Ar(t, shortcut._1)))
      and BigAnd(for (t <- originalExecution.transactions) yield Not(Vi(shortcut._1, t)))
      implies not(Encoder.DMinus(shortcut._1, shortcut._2)))
    val freeVariables = (implication.freeVars -- problem1.freeVars).toList
    val conv = EventGraphZ3Converter(param)

    Z3Prover.withZ3[Unit, Expr, Var]({ z3 =>
      for (a <- problem1.buffer) {
        a match {
          case Left(b) => z3.assume(b)
          case Right(b) => z3.comment(b)
        }
      }

      val existential = forall(freeVariables, implication)

      z3.assume(existential)
      val success =
        z3.check(timeout = Some(1000)) match {
          case Sat =>
            val model = z3.extractModel()
            log_info("Generalization check: Found a counter-example: " + DumpGraph.dumpToFile(session + ".counter-example", Encoder.toLabeledGraph(model, conv)(originalExecution, param)))
            false
          case Unsat =>
            log_debug("Generalization check: Assertion valid!")
            true
          case Unknown =>
            log_info("Generalization check: Failed without counter-example")
            false
        }

      if (param.debug) {
        log_debug("Problem size: " + z3.bookkeeper.assumptionSize)
        log_debug("Dumping smt log to " + FileSystemExporter.export(session + "_smt_log", z3.bookkeeper.smtLog.reverse.mkString("\n")))
      }

      if (success) return true
    }, conv)
    false
  }

  sealed trait ClientTransition {

    def in: TransactionID

    def out: TransactionID

    def stripClient: ClientTransition

    def toSet: Set[TransactionID] = Set(in, out)

  }

  /**
    * Represents a path of a DSG through a client, consisting of a transaction
    * with an incoming edge and a transaction with an outgoing edge.
    *
    * @param in  txn with incoming edge
    * @param out txn with outgoing edge
    */
  case class DoubleClientTransition(in: TransactionID, out: TransactionID) extends ClientTransition {

    def stripClient = DoubleClientTransition(ClientNameMaker.deconstruct(in)._1, ClientNameMaker.deconstruct(out)._1)

  }

  case class SingleClientTransition(txn: TransactionID) extends ClientTransition {

    def in: TransactionID = txn

    def out: TransactionID = txn

    def stripClient = SingleClientTransition(ClientNameMaker.deconstruct(txn)._1)

  }

  def clientTransitions(g: AbstractHistory): Set[ClientTransition] =
    g.transactionOrder.map(x => DoubleClientTransition(x.sourceID, x.targetID)).toSet ++
      g.transactions.map(x => SingleClientTransition(x))

  class CollectingAssume extends Assume[Expr] {

    def --(other: CollectingAssume): CollectingAssume = {
      val remove = other.buffer.toSet
      val res = new CollectingAssume
      res.buffer.appendAll(buffer.filterNot(remove.contains))
      res
    }

    val buffer: ListBuffer[Either[Expr, String]] = scala.collection.mutable.ListBuffer[Either[Expr, String]]()

    override def comment(str: String): Unit = buffer.append(Right(str))

    override def assume(term: Expr): Unit = buffer.append(Left(term))

    def assumes: List[Expr] = buffer.collect { case Left(x) => x }.toList

    def freeVars: Set[Var] = assumes.flatMap(_.freeVars).toSet

    def toExpr: Expr = ExprBuilder.bigAnd(assumes)

    override def toString: String = buffer.mkString("\n")
  }

  def debug(
      originalExecution: AbstractHistory,
      shortcuttingExecution: AbstractHistory,
      originalPathEdge1: (TransactionID, TransactionID),
      originalPathEdge2: (TransactionID, TransactionID),
      shortcut: (TransactionID, TransactionID),
      session: String
  )(implicit param: Parameters): Unit = {

    if (param.debug) {
      val g1 = originalExecution.toLabeledGraph
      val g2 = shortcuttingExecution.toLabeledGraph
      val g3 = (originalExecution unionOverlapping shortcuttingExecution).toLabeledGraph

      {
        val n1 = g1.nodes.zipWithIndex.find(_._1 == TransactionNames.make(originalPathEdge1._1)).get._2
        val n2 = g1.nodes.zipWithIndex.find(_._1 == TransactionNames.make(originalPathEdge1._2)).get._2
        val n3 = g1.nodes.zipWithIndex.find(_._1 == TransactionNames.make(originalPathEdge2._1)).get._2
        val n4 = g1.nodes.zipWithIndex.find(_._1 == TransactionNames.make(originalPathEdge2._2)).get._2
        val n5 = g2.nodes.zipWithIndex.find(_._1 == TransactionNames.make(shortcut._1)).get._2
        val n6 = g2.nodes.zipWithIndex.find(_._1 == TransactionNames.make(shortcut._2)).get._2

        g1.addEdge(n1, n2, Some("dsg"))
        g1.addEdge(n3, n4, Some("adep"))
        g2.addEdge(n5, n6, Some("adep"))
      }

      {
        val n1 = g3.nodes.zipWithIndex.find(_._1 == TransactionNames.make(originalPathEdge1._1)).get._2
        val n2 = g3.nodes.zipWithIndex.find(_._1 == TransactionNames.make(originalPathEdge1._2)).get._2
        val n3 = g3.nodes.zipWithIndex.find(_._1 == TransactionNames.make(originalPathEdge2._1)).get._2
        val n4 = g3.nodes.zipWithIndex.find(_._1 == TransactionNames.make(originalPathEdge2._2)).get._2
        val n5 = g3.nodes.zipWithIndex.find(_._1 == TransactionNames.make(shortcut._1)).get._2
        val n6 = g3.nodes.zipWithIndex.find(_._1 == TransactionNames.make(shortcut._2)).get._2

        g3.addEdge(n1, n2, Some("dsg"))
        g3.addEdge(n3, n4, Some("adep"))
        g3.addEdge(n5, n6, Some("adep"))
      }

      log_debug(DumpGraph.dumpToFile(session + "clientcheck1", g1))
      log_debug(DumpGraph.dumpToFile(session + "clientcheck2", g2))
      log_debug(DumpGraph.dumpToFile(session + "clientcheck3", g3))
    }
  }

}
