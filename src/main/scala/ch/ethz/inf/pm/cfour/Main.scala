/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package ch.ethz.inf.pm.cfour

import ch.ethz.inf.pm.cfour.output.{ClassificationMap, Logger}

object Main extends Logger {

  def main(args: Array[String]): Unit = {

    implicit var params: Parameters = Parameters.default

    var input: Option[String] = None
    var quickCheck: Boolean = false
    var unfoldBeforeAnalysis: Boolean = true

    val inputFile = """-inputFile=(.*)""".r
    val exportDirectory = """-exportDir=(.*)""".r
    val classificationFile = """-classificationFile=(.*)""".r

    args foreach {
      case "-println" =>
        params = params.copy(outputViaPrintln = true)
      case "-stats" =>
        params = Parameters.setStats(params)
      case "-debug" =>
        params = Parameters.setDebug(params)
      case "-quickCheck" =>
        quickCheck = true
      case "-noUnfold" =>
        unfoldBeforeAnalysis = false
      case "-cassandraFiltered" =>
        params = Parameters.setCassandraFiltered(params)
      case "-cassandraUnfiltered" =>
        params = Parameters.setCassandraUnfiltered(params)
      case "-touchdevelopFiltered" =>
        params = Parameters.setTouchDevelopFiltered(params)
      case "-touchdevelopUnfiltered" =>
        params = Parameters.setTouchDevelopUnfiltered(params)
      case classificationFile(file) =>
        params = params.copy(classificationMap = Some(ClassificationMap.loadFromFile(file)))
      case exportDirectory(dir) =>
        params = params.copy(exportDirectory = Some(dir))
      case inputFile(f) =>
        input = Some(f)
      case "-disableProcesses" =>
        params = params.copy(encodeProcesses = false)
      case "-disableConstraints" =>
        params = params.copy(encodeConstraints = false)
      case "-disableAbsorption" =>
        params = params.copy(encodeAbsorption = false)
      case "-disableCommutativity" =>
        params = params.copy(encodeCommutativity = false)
      case "-dumpViolationIDs" =>
        params = params.copy(quiet = true, dumpViolationIDs = true)
      case "-disableDisplayCode" =>
        params = params.copy(ignoreDisplayCode = false)
      case "-disableAtomicSets" =>
        params = params.copy(encodeNamedAtomicSets = false)
      case "-disableSubsetMinimality" =>
        params = params.copy(useStaticSubsetMinimality = false)
      case "-disableGenTest" =>
        params = params.copy(resultGeneralizationCheck = false)
      case "-help" =>
        print(
          """
            | Takes a JSON document from `stdin`.
            |
            | Allowed commandline options:
            |
            |  -exportDir=[path]        Export files to this directory
            |  -classification=[path]   Use given manual classification of warnings (for statistics)
            |  -noUnfold                Unfolds the graph before analyzing it.
            |  -debug                   Create extra debugging symbols in formula for better output, but slower solving
            |  -cassandraFiltered       Cassandra options with filtering
            |  -cassandraUnfiltered     Cassandra options without filtering
            |  -touchdevelopFiltered    TouchDevelop options with filtering
            |  -touchdevelopUnfiltered  TouchDevelop options without filtering
            |  -disableProcesses        Disable encoding of control-flow
            |  -disableConstraints      Disable encoding of constraints
            |  -disableAbsorption       Disable encoding of absorption
            |  -disableCommutativity    Disable encoding of commutativity
            |  -disableGenTest          Disable checking if result generalizes to arbitrary number of sessions
            |  -stats                   Standard options for generating stats
            |  -tsv                     Output data as TSV
            |  -quickCheck              Only run polynomial time check with unfolding
            |  -println                 Use stdout instead of the logging framework
            |
          """.stripMargin)
        sys.exit(0)
      case _ => println("Unknown option"); sys.exit(-1)
    }

    val inputGraph = AbstractHistory.fromJSON(
      input.map(io.Source.fromFile).getOrElse(io.Source.stdin).mkString
    )

    if (quickCheck) {
      assert(unfoldBeforeAnalysis)
      assert(params.classificationMap.isEmpty)
      Unfolder.unfoldAndQuickCheck(inputGraph)
    } else if (unfoldBeforeAnalysis) {
      Unfolder.unfoldAndCheck(inputGraph)
    } else {
      log_info(Encoder.findViolations(inputGraph).toString)
    }

  }

}
