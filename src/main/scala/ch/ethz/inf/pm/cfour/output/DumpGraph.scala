/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package ch.ethz.inf.pm.cfour.output

import ch.ethz.inf.pm.cfour.Parameters

/**
  *
  * Dumps an execution into an HTML/Javascript based graph.
  *
  * @author Lucas Brutschy
  */
object DumpGraph {

  def dumpToFile[Node, Weight](
      name: String,
      graph: LabeledGraph[Node, Weight]
  )(implicit param: Parameters): String = {

    FileSystemExporter.export(name + ".html", getString(graph))

  }

  /** Returns a string to a file visualizing the given graph structure */
  def getString[Node, Weight](
      graph: LabeledGraph[Node, Weight]
  ): String = {

    val nodeMap = graph.nodes.zipWithIndex.toMap[Node, Int]
    val revNodeMap = nodeMap.map(_.swap)

    val nodeStr = (graph.nodes.zipWithIndex.sortBy(x => graph.getNodeLabel(x._1)) map { case (node, id) =>
      val name = graph.getNodeLabel(node).replace("'", "\"")
      val clazz = graph.getNodeClass(node)
      val partition = graph.getPartition(node).map { x => ", parent: '" + nodeMap(x) + "'" }.getOrElse("")
      s"{ data: { id: '$id', name: '$name' $partition }, classes: '$clazz' }"
    }).mkString(",\n")

    val edgeStr = (graph.edges.toList.sortBy(x => graph.getNodeLabel(revNodeMap(x._1))) map { case (source, target, weight) =>
      val label = weight.map(graph.getEdgeLabel).getOrElse("")
      val clazz = graph.getEdgeClass((revNodeMap(source), revNodeMap(target), weight))
      val id = source + "to" + target + label
      s"{ data: { id: '$id', source: '$source', target: '$target', label: '$label' }, classes: '$clazz' }"
    }).mkString(",\n")

    val extraInformation = graph.getExtraInformation.replace("\n", "<br />")

    s"""
       |<!DOCTYPE html>
       |<html>
       |<head>
       |<meta charset=utf-8 />
       |<meta name='viewport' content='user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, minimal-ui'>
       |<title>Sessions</title>
       |<style>
       |body {
       |  font: 14px helvetica neue, helvetica, arial, sans-serif;
       |}
       |
         |#cy {
       |  height: 100%;
       |  width: 100%;
       |  position: absolute;
       |  left: 0;
       |  top: 0;
       |}
       |</style>
       |<script src='http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js'></script>
       |<script src='https://cdnjs.cloudflare.com/ajax/libs/cytoscape/2.7.17/cytoscape.min.js'></script>
       |<script src="https://cdn.rawgit.com/cpettitt/dagre/v0.7.4/dist/dagre.min.js"></script>
       |<script src="https://cdn.rawgit.com/cytoscape/cytoscape.js-dagre/1.1.2/cytoscape-dagre.js"></script>
       |<script>
       |
       |$$(function(){ // on dom ready
       |
       |var cy = cytoscape({
       |  container: document.getElementById('cy'),
       |
       |  style: [
       |    {
       |      selector: 'node',
       |      css: {
       |        'content': 'data(name)',
       |        'text-valign': 'center',
       |        'text-halign': 'center',
       |        'background-color': 'lightgray',
       |        'width': 'label',
       |        'height': 'label',
       |        'padding': '5px',
       |        'shape': 'roundrectangle'
       |      }
       |    },
       |    {
       |      selector: '$$node > node',
       |      css: {
       |        'text-valign': 'top',
       |        'text-halign': 'center'
       |      }
       |    },
       |    {
       |      selector: 'edge',
       |      css: {
       |        'target-arrow-shape': 'triangle',
       |        'background-color': 'black',
       |        'line-color': 'black',
       |        'target-arrow-color': 'black',
       |        'source-arrow-color': 'black',
       |        'content': 'data(label)',
       |        'curve-style': 'bezier'
       |      }
       |    },
       |    {
       |      selector: '.to',
       |      css: {
       |        'line-color': 'darkgreen',
       |        'target-arrow-color': 'darkgreen',
       |        'source-arrow-color': 'darkgreen'
       |      }
       |    },
       |    {
       |      selector: '.po',
       |      css: {
       |        'line-color': 'darkblue',
       |        'target-arrow-color': 'darkblue',
       |        'source-arrow-color': 'darkblue'
       |      }
       |    },
       |    {
       |      selector: '.violation',
       |      css: {
       |        'line-color': 'red',
       |        'target-arrow-color': 'red',
       |        'source-arrow-color': 'red'
       |      }
       |    },
       |    {
       |      selector: '.path',
       |      css: {
       |        'line-color': 'blue',
       |        'target-arrow-color': 'blue',
       |        'source-arrow-color': 'blue'
       |      }
       |    },
       |    {
       |      selector: '.ca',
       |      css: {
       |        'line-color': 'LightSlateGray',
       |        'target-arrow-color': 'LightSlateGray',
       |        'source-arrow-color': 'LightSlateGray'
       |      }
       |    },
       |    {
       |      selector: '.ar',
       |      css: {
       |        'line-color': 'DarkSlateGray',
       |        'target-arrow-color': 'DarkSlateGray',
       |        'source-arrow-color': 'DarkSlateGray'
       |      }
       |    },
       |    {
       |      selector: '.event',
       |      css: {
       |        'background-color': 'lightcoral'
       |      }
       |    },
       |    {
       |      selector: '.inactive',
       |      css: {
       |        'line-color': 'grey',
       |        'target-arrow-color': 'grey',
       |        'source-arrow-color': 'grey',
       |        'background-color': 'grey'
       |      }
       |    },
       |    {
       |      selector: '.txn',
       |      css: {
       |        'background-color': 'lightgrey'
       |      }
       |    },
       |    {
       |      selector: '.client',
       |      css: {
       |        'background-color': 'darkgrey'
       |      }
       |    },
       |    {
       |      selector: '.aux',
       |      css: {
       |        'background-color': 'grey'
       |      }
       |    },
       |    {
       |      selector: ':selected',
       |      css: {
       |        'background-color': 'yellow',
       |        'line-color': 'yellow',
       |        'target-arrow-color': 'yellow',
       |        'source-arrow-color': 'yellow'
       |      }
       |    }
       |  ],
       |
         |  elements: {
       |    nodes: [
       |      $nodeStr
       |    ], edges: [
       |      $edgeStr
       |    ]
       |  },
       |
       |  layout: {
       |    name: 'dagre'
       |  }
       |});
       |
         |}); // on dom ready
       |</script>
       |
         |</head>
       |<body>
       |
         |<div id='cy'></div>
       |  <div>$extraInformation</div>
       |</body>
       |</html>
    """.stripMargin

  }

}
