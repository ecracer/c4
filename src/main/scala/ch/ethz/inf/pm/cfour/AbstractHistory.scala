/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package ch.ethz.inf.pm.cfour

import ch.ethz.inf.pm.cfour.Encoder._
import ch.ethz.inf.pm.cfour.output.{DumpGraph, LabeledGraph}
import ch.ethz.inf.pm.cfour.utils.AbstractGraph
import salat.annotations.Salat
import salat.{TypeHintStrategy, _}

import scala.util.matching.Regex

object AbstractHistory {

  type OperationID = String
  type TransactionID = String
  type EventID = String

  def fromJSON(a: String): AbstractHistory = {
    grater[AbstractHistory].fromJSON(a)
  }

  implicit val ctx: Context {
    val typeHintStrategy: TypeHintStrategy

    val suppressDefaultArgs: Boolean

    val name: OperationID
  } = new Context {
    val name = "WithoutPackage"
    override val suppressDefaultArgs = true
    override val typeHintStrategy: TypeHintStrategy = new TypeHintStrategy {
      override val when: salat.TypeHintFrequency.Value = TypeHintFrequency.WhenNecessary
      override val typeHint: String = "_t"
      lazy val pkg: String = this.getClass.getPackage.getName + "."

      def decode(in: Any): String = in match {
        case s: String if s != null => pkg + s
        case x => throw new Error("Can't encode supplied value '%s'".format(x))
      }

      def encode(in: String): String = {
        assert(in.startsWith(pkg))
        in.stripPrefix(pkg)
      }
    }
  }

}

import ch.ethz.inf.pm.cfour.AbstractHistory._

final case class AbstractHistory(
    events: List[Event],
    programOrder: List[PoEdge],
    transactionOrder: List[ToEdge],
    system: SystemSpecification,
    globalConstraint: Expr = True
) {


  if (Parameters.get.debug) {
    for (e <- events) {
      assert(!e.displayCode || operationMap(e.operationID).isInstanceOf[Query])
      assert(!e.constraint.freeVars.exists { case _: ArgRight => true; case _ => false })
    }
    assert(events.map(_.id).distinct.size == events.map(_.id).size)
    assert(programOrder forall { x => eventMap.contains(x.sourceID) && eventMap.contains(x.targetID) })
    for (t <- transactions) {
      val pov = programOrderView(t)

      def dumpViolation =
        DumpGraph.dumpToFile("violation", this.toLabeledGraph(Parameters.get))(Parameters.get)

      assert(pov.hasSingleSource, "Transactions does not have single source: " + dumpViolation)
      assert(pov.hasSingleSink, "Transactions does not have single sink: " + dumpViolation)
      assert(pov.isConnected, "Transactions is not connected : " + dumpViolation)
    }
    assert(programOrder forall { x => eventMap(x.sourceID).txn == eventMap(x.targetID).txn })
    assert(transactionOrder forall { x =>
      (x.sourceID == "init" || (transactions contains x.sourceID)) &&
        (transactions contains x.targetID)
    })
    assert(globalConstraint.freeVars forall {
      case _: EventArgVar => false
      case _: ArgLeft => false
      case _: ArgRight => false
      case _: EdgeVariable => false
      case _ => true
    })
  }

  lazy val eventMap: Map[String, Event] =
    events.map { x => (x.id, x) }.toMap

  lazy val operationMap: Map[String, Operation] =
    system.operations.map { x => (x.id, x) }.toMap

  lazy val updates: List[Event] =
    events.filter(x => x.operationID != Skip.id && operationMap(x.operationID).isInstanceOf[Update])

  lazy val queries: List[Event] =
    events.filter(x => x.operationID != Skip.id && operationMap(x.operationID).isInstanceOf[Query])

  lazy val transactions: List[TransactionID] =
    (events.map(_.txn).toSet ++ transactionOrder.map(_.sourceID).toSet ++ transactionOrder.map(_.targetID).toSet).toList

  lazy val eventsByTxn: Map[TransactionID, List[Event]] =
    events.groupBy(_.txn)

  lazy val updatesByTxn: Map[TransactionID, List[Event]] =
    updates.groupBy(_.txn)

  lazy val queriesByTxn: Map[TransactionID, List[Event]] =
    queries.groupBy(_.txn)

  lazy val eventsByOp: Map[OperationID, List[Event]] =
    events.groupBy(_.operationID)

  lazy val programOrderByTransaction: Map[TransactionID, List[PoEdge]] =
    programOrder.groupBy(_.txn(this))

  lazy val programOrderBySource: Map[EventID, List[PoEdge]] =
    programOrder.groupBy(_.sourceID)

  lazy val programOrderByTarget: Map[EventID, List[PoEdge]] =
    programOrder.groupBy(_.targetID)

  lazy val programOrderView: Map[TransactionID, AbstractGraph[EventID]] =
    transactions.map(x => (x, makeProgramOrderView(x))).toMap

  lazy val transactionOrderView: AbstractGraph[TransactionID] =
    makeTransactionOrderView

  lazy val transactionOrderBySource: Map[EventID, List[ToEdge]] =
    transactionOrder.groupBy(_.sourceID)

  lazy val transitiveProgramOrderView: Map[TransactionID, AbstractGraph[EventID]] =
    transactions.map(x => (x, makeTransitiveProgramOrderView(x))).toMap

  def possiblyNonCommutativeSMT(e1: Event, e2: Event)(implicit param: Parameters, g: AbstractHistory): Boolean = {
    import ExprBuilder._
    if ((e1 commutesWith e2) == True) return false
    if (param.checkCommutativityAbsorptionWithSMT) {
      if (Cache.nonSatisfiable.contains((e1, e2, true))) true
      else if (Cache.nonSatisfiable.contains((e1, e2, false))) false
      else {
        val conv = EventGraphZ3Converter(param)
        Z3Prover.withZ3[Boolean, Expr, Var]({ z3 =>
          val ce1 = e1.instantiateToClient(0)
          val ce2 = e2.instantiateToClient(1)
          z3.assume(not(ce1 commutesWith ce2))
          z3.assume(ce1.constraint.instantiate(ce1))
          z3.assume(ce2.constraint.instantiate(ce2))
          z3.assume(bigOrDefaultTrue(for (x <- programOrderBySource.getOrElse(e1.id, Nil)) yield {
            x.constraint.instantiate(ce1, x.target.instantiateToClient(0))
          }))
          z3.assume(bigOrDefaultTrue(for (x <- programOrderByTarget.getOrElse(ce1.id, Nil)) yield {
            x.constraint.instantiate(x.source.instantiateToClient(0), ce1)
          }))
          z3.assume(bigOrDefaultTrue(for (x <- programOrderBySource.getOrElse(ce2.id, Nil)) yield {
            x.constraint.instantiate(ce2, x.target.instantiateToClient(1))
          }))
          z3.assume(bigOrDefaultTrue(for (x <- programOrderByTarget.getOrElse(ce2.id, Nil)) yield {
            x.constraint.instantiate(x.source.instantiateToClient(1), ce2)
          }))
          z3.check() match {
            case Z3Prover.Sat =>
              Cache.nonSatisfiable += ((e1, e2, true))
              true
            case Z3Prover.Unknown =>
              log_info("ERROR: Could not solve")
              false
            case Z3Prover.Unsat =>
              Cache.nonSatisfiable += ((e1, e2, false))
              false
          }
        }, conv)
      }
    } else true
  }


  def possiblyNotAbsorbedBySMT(e1: Event, e2: Event)(implicit param: Parameters, g: AbstractHistory): Boolean = {
    import ExprBuilder._
    if ((e1 absorbedBy e2) == True) return false
    if (param.checkCommutativityAbsorptionWithSMT) {
      if (Cache.nonAbsorbedBy.contains((e1, e2, true))) true
      else if (Cache.nonAbsorbedBy.contains((e1, e2, false))) false
      else {
        val conv = EventGraphZ3Converter(param)
        Z3Prover.withZ3[Boolean, Expr, Var]({ z3 =>
          val ce1 = e1.instantiateToClient(0)
          val ce2 = e2.instantiateToClient(1)
          z3.assume(not(ce1 absorbedBy ce2))
          z3.assume(ce1.constraint.instantiate(ce1))
          z3.assume(ce2.constraint.instantiate(ce2))
          z3.assume(bigOrDefaultTrue(for (x <- programOrderBySource.getOrElse(e1.id, Nil)) yield {
            x.constraint.instantiate(ce1, x.target.instantiateToClient(0))
          }))
          z3.assume(bigOrDefaultTrue(for (x <- programOrderByTarget.getOrElse(ce1.id, Nil)) yield {
            x.constraint.instantiate(x.source.instantiateToClient(0), ce1)
          }))
          z3.assume(bigOrDefaultTrue(for (x <- programOrderBySource.getOrElse(ce2.id, Nil)) yield {
            x.constraint.instantiate(ce2, x.target.instantiateToClient(1))
          }))
          z3.assume(bigOrDefaultTrue(for (x <- programOrderByTarget.getOrElse(ce2.id, Nil)) yield {
            x.constraint.instantiate(x.source.instantiateToClient(1), ce2)
          }))
          z3.check() match {
            case Z3Prover.Sat =>
              Cache.nonAbsorbedBy += ((e1, e2, true))
              true
            case Z3Prover.Unknown =>
              log_info("ERROR: Could not solve")
              false
            case Z3Prover.Unsat =>
              Cache.nonAbsorbedBy += ((e1, e2, false))
              false
          }
        }, conv)
      }
    } else true
  }

  lazy val updateOps: List[Operation] = system.operations.filter(_.isUpdate)
  lazy val queryOps: List[Operation] = system.operations.filter(_.isQuery)

  def toJSON: String = {
    grater[AbstractHistory].toPrettyJSON(this)
  }

  def ++(other: AbstractHistory): AbstractHistory = {

    import ExprBuilder._

    assert(this.system == other.system)
    assert((this.events.toSet intersect other.events.toSet).isEmpty)
    assert((this.transactions.toSet intersect other.transactions.toSet).isEmpty)

    AbstractHistory(
      this.events ::: other.events,
      this.programOrder ::: other.programOrder,
      this.transactionOrder ::: other.transactionOrder,
      this.system,
      this.globalConstraint and other.globalConstraint
    )

  }

  def removeUniques(): AbstractHistory = {
    var uniqueCounter = 0
    this.copy(
      events = events.map { x =>
        x.copy(
          constraint = x.constraint.replace {
            case FreshUniqueVar(s) =>
              uniqueCounter = uniqueCounter + 1
              AuxVar(s, "alt_unique_" + uniqueCounter)
            case x: Expr => x
          })
      }
    )
  }


  def unionOverlapping(other: AbstractHistory): AbstractHistory = {
    import ExprBuilder._
    AbstractHistory(
      (this.events ::: other.events).distinct,
      (this.programOrder ::: other.programOrder).distinct,
      (this.transactionOrder ::: other.transactionOrder).distinct,
      this.system,
      this.globalConstraint and other.globalConstraint
    )
  }

  def extendAndReduce(
      eventsToAdd: List[Event] = Nil,
      poToAdd: List[PoEdge] = Nil,
      toToAdd: List[ToEdge] = Nil,
      eventsToRemove: Set[Event] = Set.empty,
      poToRemove: Set[PoEdge] = Set.empty,
      toToRemove: Set[ToEdge] = Set.empty
  ): AbstractHistory = {

    assert((eventsToAdd intersect this.events).isEmpty)
    assert((poToAdd intersect this.programOrder).isEmpty)
    assert((toToAdd intersect this.transactionOrder).isEmpty)
    assert(eventsToRemove subsetOf this.events.toSet)
    assert(poToRemove subsetOf this.programOrder.toSet)
    assert(toToRemove subsetOf this.transactionOrder.toSet)

    this.copy(
      events = (eventsToAdd ::: this.events).filter(!eventsToRemove.contains(_)),
      programOrder = (poToAdd ::: this.programOrder).filter(!poToRemove.contains(_)),
      transactionOrder = (toToAdd ::: this.transactionOrder).filter(!toToRemove.contains(_))
    )
  }


  /**
    * Returns the reflexive closure of the graph
    */
  def reflexivelyCloseTransactionOrder: AbstractHistory = {
    this.copy(transactionOrder = (transactionOrder ::: transactions.map(x => ToEdge(x, x))).distinct)
  }

  def fullTransactionOrder: AbstractHistory = {
    this.copy(transactionOrder = for (src <- transactions; tgt <- transactions) yield {ToEdge(src,tgt)})
  }

  def sortedTransactionOrder: AbstractHistory = {
    this.copy(transactionOrder = transactionOrder.sortBy(x => (x.sourceID,x.targetID,x.isSynchronized)))
  }

  /**
    * Returns the transitive closure of the graph, taking care that the "synchronized program order"
    * is computed correctly
    *
    * TODO: This is horribly inefficient
    */
  def transitivelyCloseTransactionOrder: AbstractHistory = {

    def rec(freshEdges: List[ToEdge], allEdges: List[ToEdge]): List[ToEdge] = {

      if (freshEdges.isEmpty) return allEdges

      var newEdges = List.empty[ToEdge]
      for (e1 <- allEdges; e2 <- freshEdges
           if e1.targetID == e2.sourceID
             && !allEdges.exists(x => x.sourceID == e1.sourceID && x.targetID == e2.targetID)) yield {

        newEdges ::= ToEdge(
          e1.sourceID,
          e2.targetID,
          isSynchronized = e1.isSynchronized || e2.isSynchronized
        )

      }

      rec(newEdges, allEdges ::: newEdges)

    }

    this.copy(transactionOrder = rec(transactionOrder, transactionOrder))

  }

  def pruneEvents(f: Event => Boolean): AbstractHistory = {

    implicit val graph: AbstractHistory = this

    val prunedProgramOrder =
      events.foldLeft(programOrder)((po, e) =>
        if (f(e))
          po.flatMap {
            edge =>
              if (edge.sourceID == e.id && edge.targetID == e.id) {
                Nil
              } else if (edge.sourceID == e.id) {
                po.filter(y => y.targetID == edge.sourceID && !f(y.source)).map(y => y.copy(targetID = edge.targetID))
              } else if (edge.targetID == e.id) {
                po.filter(y => y.sourceID == edge.targetID && !f(y.target)).map(y => y.copy(sourceID = edge.sourceID))
              } else {
                List(edge)
              }
          }
        else po
      )

    this.copy(
      events.filterNot(f),
      prunedProgramOrder,
      transactionOrder,
      system,
      globalConstraint
    )
  }

  def restrictToTransaction(restriction: TransactionID): AbstractHistory = {

    assert(transactions contains restriction)

    AbstractHistory(
      this.events.filter(restriction == _.txn),
      this.programOrderByTransaction.getOrElse(restriction, Nil),
      Nil,
      this.system,
      this.globalConstraint
    )

  }


  def renameTransactions(C: String => String): AbstractHistory = {

    def renameTransactionLocalVars(z: Expr) = z.replace {
      case x: TransactionLocalVar => x.copy(txn = C(x.txn))
      case x: UniqueTransactionLocalVar => x.copy(txn = C(x.txn))
      case x: Expr => x
    }

    this.copy(

      events = this.events.map { x =>
        x.copy(
          id = C(x.id),
          txn = C(x.txn),
          constraint = renameTransactionLocalVars(x.constraint)
        )
      },

      programOrder =
        this.programOrder.map { x =>
          x.copy(
            sourceID = C(x.sourceID),
            targetID = C(x.targetID),
            constraint = renameTransactionLocalVars(x.constraint)
          )
        },

      transactionOrder =
        this.transactionOrder.map{ x =>
          x.copy(
            sourceID = C(x.sourceID),
            targetID = C(x.targetID)
          )
        }

    )
  }

  def duplicateTransactions(C1: String => String, C2: String => String): AbstractHistory = {
    this.renameTransactions(C1) ++ this.renameTransactions(C2)
  }

  def restrictAndUnfoldTransactionEdge(
      restriction: ToEdge,
      rename1: Option[String => String] = None,
      rename2: Option[String => String] = None): AbstractHistory = {

    assert(transactionOrder contains restriction)

    val restricted =
      AbstractHistory(
        this.events.filter(x => restriction.contains(x.txn)),
        this.programOrder.filter { x =>
          restriction.contains(x.txn(this))
        },
        List(restriction),
        this.system,
        this.globalConstraint
      )

    if (restriction.sourceID == restriction.targetID) {
      assert(restricted.transactions.size == 1)

      def C1(s: String): String = rename1.map(_ (s)).getOrElse("it1_" + s)

      def C2(s: String): String = rename2.map(_ (s)).getOrElse("it2_" + s)

      restricted.duplicateTransactions(C1, C2).copy(transactionOrder = Nil) +
        restriction.copy(C1(restriction.sourceID), C2(restriction.sourceID))
    } else {
      restricted
    }

  }

  def +(e: ToEdge): AbstractHistory = {
    this.copy(transactionOrder = e :: transactionOrder)
  }

  def source(txn: TransactionID): EventID = {
    programOrderView(txn).sources.head
  }

  def sink(txn: TransactionID): EventID = {
    programOrderView(txn).sinks.head
  }

  def txn: TransactionID = {
    assert(transactions.size == 1)
    transactions.head
  }

  def txnSink: TransactionID = {
    assert(transactionOrderView.sinks.size == 1)
    transactionOrderView.sinks.head
  }

  def txnSource: TransactionID = {
    assert(transactionOrderView.sources.size == 1)
    transactionOrderView.sources.head
  }

  def isProgramOrdered(e: Event, e2: Event): Boolean = {
    programOrderBySource.getOrElse(e.id, Set.empty).exists(_.targetID == e2.id)
  }

  def isTransactionOrdered(e: TransactionID, e2: TransactionID): Boolean = {
    transactionOrderBySource.getOrElse(e, Set.empty).exists(_.targetID == e2)
  }

  def getTransactionOrder(e: TransactionID, e2: TransactionID): Option[ToEdge] = {
    transactionOrderBySource.getOrElse(e, Set.empty).find(_.targetID == e2)
  }

  def isTransitivelyProgramOrdered(txn: TransactionID, u: Event, v: Event): Boolean = {
    transitiveProgramOrderView(txn).hasEdge(u.id, v.id)
  }

  def enumeratePaths(txn: TransactionID): List[List[PoEdge]] = {
    val eventSet: Set[EventID] = eventsByTxn(txn).map(_.id).toSet
    val sources = eventSet -- programOrderByTarget.keySet

    def rec(last: EventID): List[List[PoEdge]] = {
      programOrderBySource.getOrElse(last, Nil) match {
        case Nil => List(List.empty[PoEdge])
        case next: List[PoEdge] => next.flatMap(n => rec(n.targetID).map(n :: _))
      }
    }

    sources.flatMap(rec).toList
  }

  def instantiateToClient(c: Int, other: Option[Int]): AbstractHistory = {

    this.copy(
      events = events map {
        _.instantiateToClient(c)
      },
      programOrder = programOrder map {
        _.instantiateToClient(c)
      },
      transactionOrder = transactionOrder map {
        _.instantiateToClient(c)
      },
      globalConstraint = globalConstraint.instantiateToClient(c, other)
    )

  }

  def makeTransactionOrderView: AbstractGraph[TransactionID] =
    new AbstractGraph[TransactionID] {
      override def vertices: List[TransactionID] =
        transactions

      override def successors(n: TransactionID): List[TransactionID] =
        transactionOrderBySource.getOrElse(n, Nil).map(_.targetID)
    }

  def makeProgramOrderView(txn: TransactionID): AbstractGraph[EventID] =
    new AbstractGraph[EventID] {
      override def vertices: List[EventID] =
        eventsByTxn.getOrElse(txn, Nil).map(_.id)

      override def successors(n: EventID): List[EventID] =
        programOrderBySource.getOrElse(n, Nil).map(_.targetID)
    }

  def makeTransitiveProgramOrderView(txn: TransactionID): AbstractGraph[EventID] =
    programOrderView(txn).toSimpleGraph.transitiveClosure

  def toLabeledGraph(implicit param: Parameters): LabeledGraph[String, String] = {

    val pg = new LabeledGraph[String, String]()
    val ec = EventGraphZ3Converter(param)

    val Client = """.*client(\d)""".r
    val clients = transactions.collect { case Client(s) => s.toInt }.toSet
    clients.foreach {
      x =>
        val node = "client_" + x
        pg.addNode(node)
        pg.setNodeLabel(node, "Client " + x)
        pg.setNodeClass(node, "client")
    }

    transactions.foreach {
      x =>
        val node = TransactionNames.make(x)
        pg.addNode(node)
        pg.setNodeLabel(node, "txn:" + x.toString)
        pg.setNodeClass(node, "txn")
        x match {
          case Client(s) =>
            pg.addPartitioning(node, "client_" + s)
          case _ => ()
        }
    }

    events.foreach {
      x =>
        val node = x.id
        node -> pg.addNode(node)
        pg.setNodeLabel(node, x.id + ":" + x.operationID + " (" + ec.encode(x.constraint) + ")")
        eventMap.get(node).foreach {
          x =>
            val txn = TransactionNames.make(x.txn)
            pg.addPartitioning(node, txn)
        }
        pg.setNodeClass(node, "event")
    }

    programOrder.foreach { x =>
      val weight = if (x.constraint != True) Some(ec.encode(x.constraint)) else None
      pg.addEdge(
        x.sourceID,
        x.targetID,
        weight
      )
      pg.setEdgeClass((x.sourceID, x.targetID, weight), "po")
    }

    transactionOrder.foreach { x =>
      val weight = if (x.isSynchronized) Some("to_synchronized") else Some("to")
      pg.addEdge(
        TransactionNames.make(x.sourceID),
        TransactionNames.make(x.targetID),
        weight
      )
      pg.setEdgeClass((TransactionNames.make(x.sourceID), TransactionNames.make(x.targetID), weight), "to")
    }

    pg.addExtraInformation(ec.encode(globalConstraint))

    pg
  }

}

// Commutativity/Absorpttion satisfiable by event
// TODO: This should go somewhere else
object Cache {

  var nonSatisfiable: Set[(Event, Event, Boolean)] = Set.empty
  var nonAbsorbedBy: Set[(Event, Event, Boolean)] = Set.empty

  def reset(): Unit = {
    nonSatisfiable = Set.empty
    nonAbsorbedBy = Set.empty
  }

}

trait ConstantPrefixNameMaker {

  def prefix: String

  def make(r: TransactionID): String =
    s"$prefix$r"

  def isName(a: String): Boolean =
    a.startsWith(prefix)

  def deconstruct(str: String): TransactionID = {
    str.stripPrefix(prefix)
  }

}

object TransactionNames extends ConstantPrefixNameMaker {
  def prefix = "txn_"
}


object ClientNameMaker {

  val ClientName: Regex = """(.*)_client(\d+)""".r

  def make(str: String, c: Int) = s"${str}_client$c"

  def isName(str: String): Boolean = {
    str match {
      case ClientName(_, _) => true
      case _ => false
    }
  }

  def deconstruct(str: String): (String, Int) = {
    str match {
      case ClientName(a, b) => (a, b.toInt)
      case _ => throw new IllegalArgumentException("given string is not instantiated to a client: " + str)
    }
  }

}

@Salat
sealed trait Expr {

  def instantiateToClient(c: Int, other: Option[Int] = None): Expr = {
    replace({
      case a: ClientLocalVar => a.bind(c)
      case a: OtherClientLocalVar =>
        other match {
          case Some(x) =>
            a.bind(x)
          case None =>
            throw new UnsupportedOperationException("client relational constraint outside of global constraint")
        }
      case x => x
    })
  }

  def instantiate(me: Event): Expr =
    replace({
      case a: ArgLeft => a.bind(me)
      case x => x
    })

  def instantiate(l: Event, r: Event): Expr =
    replace({
      case a: ArgLeft => a.bind(l)
      case a: ArgRight => a.bind(r)
      case x => x
    })

  def instantiate(l: Event, m: Event, r: Event): Expr =
    replace({
      case a: ArgLeft => a.bind(l)
      case a: ArgMiddle => a.bind(m)
      case a: ArgRight => a.bind(r)
      case x => x
    })

  def swap: Expr =
    replace({
      case ArgLeft(n, s) => ArgRight(n, s)
      case ArgRight(n, s) => ArgLeft(n, s)
      case x => x
    })

  def replace(f: Expr => Expr): Expr

  def remove(f: Expr => Boolean): Expr = {
    this match {
      case And(l, r) => if (f(l) && f(r)) True else if (f(l)) r else if (f(r)) l else this
      case BigAnd(es) => ExprBuilder.bigAnd(es.filterNot(f))
      case _ => if (f(this)) True else this
      // TODO: This is just special casing
    }
  }

  def freeVars: Set[Var]

}

@Salat
sealed trait UnOp extends Expr {
  val e: Expr

  def factory(e: Expr): Expr

  override def replace(f: (Expr) => Expr): Expr = f(factory(e.replace(f)))

  override def freeVars: Set[Var] = e.freeVars
}

@Salat
sealed trait BinOpExpr extends Expr {
  val l: Expr
  val r: Expr

  def factory(l: Expr, r: Expr): Expr

  override def replace(f: (Expr) => Expr): Expr = f(factory(l.replace(f), r.replace(f)))

  override def freeVars: Set[Var] = l.freeVars ++ r.freeVars
}

@Salat
sealed trait ArbOp extends Expr {
  val es: List[Expr]

  def factory(es: List[Expr]): Expr

  override def replace(f: (Expr) => Expr): Expr = f(factory(es map {
    _.replace(f)
  }))

  override def freeVars: Set[Var] = if (es.isEmpty) Set.empty else es.map(_.freeVars).reduce(_ ++ _)
}

@Salat
sealed trait Constant extends Expr {
  override def replace(f: (Expr) => Expr): Expr = f(this)

  override def freeVars: Set[Var] = Set.empty
}

final case class Implies(l: Expr, r: Expr) extends BinOpExpr {
  override def factory(l: Expr, r: Expr) = Implies(l, r)
}

final case class Iff(l: Expr, r: Expr) extends BinOpExpr {
  override def factory(l: Expr, r: Expr) = Iff(l, r)
}

final case class And(l: Expr, r: Expr) extends BinOpExpr {
  override def factory(l: Expr, r: Expr) = And(l, r)
}

final case class Or(l: Expr, r: Expr) extends BinOpExpr {
  override def factory(l: Expr, r: Expr) = Or(l, r)
}

final case class Equal(l: Expr, r: Expr) extends BinOpExpr {
  override def factory(l: Expr, r: Expr) = Equal(l, r)
}

final case class LessEqual(l: Expr, r: Expr) extends BinOpExpr {
  override def factory(l: Expr, r: Expr) = LessEqual(l, r)
}

final case class GreaterEqual(l: Expr, r: Expr) extends BinOpExpr {
  override def factory(l: Expr, r: Expr) = GreaterEqual(l, r)
}

final case class Unequal(l: Expr, r: Expr) extends BinOpExpr {
  override def factory(l: Expr, r: Expr) = Unequal(l, r)
}

trait Quantifier extends UnOp {

  val vs: List[Var]

  override def freeVars: Set[Var] =
    e.freeVars -- vs

}

final case class NamedAtomicSetChosenArgument(atomicSet: String, record: String, n: Int, sort: Sort) extends Var

final case class AtomicSetPredicate(operations: List[OperationID], e: Expr)

final case class Forall(vs: List[Var], e: Expr) extends Quantifier {
  override def factory(e: Expr) = Forall(vs, e)
}


final case class Exists(vs: List[Var], e: Expr) extends Quantifier {
  override def factory(e: Expr) = Exists(vs, e)
}

final case class Not(e: Expr) extends UnOp {
  override def factory(e: Expr) = Not(e)
}

final case class BigOr(es: List[Expr]) extends ArbOp {
  override def factory(es: List[Expr]) = BigOr(es)
}

final case class BigAnd(es: List[Expr]) extends ArbOp {
  override def factory(es: List[Expr]) = BigAnd(es)
}

final case class BigXor(es: List[Expr]) extends ArbOp {
  override def factory(es: List[Expr]) = BigXor(es)
}

final case class StringConst(str: String) extends Constant

final case class IntConst(i: Int) extends Constant

final case class RealConst(d: Double) extends Constant

case object True extends Constant

case object False extends Constant

@Salat
trait Var extends Expr {
  def sort: Sort

  def replace(f: Expr => Expr): Expr = f(this)

  def freeVars: Set[Var] = Set(this)
}

case class AuxVar(sort: Sort, s: String) extends Var

case class TxnAuxVar(sort: Sort, s: String, txn: String) extends Var

case class FreshUniqueVar(sort: Sort) extends Var

@Salat
sealed trait Sort {
  def emptyConstant: Expr
}

case object BoolSort extends Sort {
  def emptyConstant: False.type = False
}

case object StringSort extends Sort {
  def emptyConstant = StringConst("")
}

case object IntSort extends Sort {
  def emptyConstant = IntConst(0)
}

case object RealSort extends Sort {
  def emptyConstant: RealConst = RealConst(0)
}

@Salat
sealed trait ArgVar extends Var {
  val n: Int

  def bind(e: Event): Var = EventArgVar(n, e.id, sort)
}

/** for defining binary/ternary relations */
case class ArgLeft(n: Int, sort: Sort) extends ArgVar

/** for defining binary/ternary relations */
case class ArgRight(n: Int, sort: Sort) extends ArgVar

/** for defining ternary relations */
case class ArgMiddle(n: Int, sort: Sort) extends ArgVar

final case class EventArgVar(n: Int, e: String, sort: Sort) extends Var

case class GlobalVar(id: String, sort: Sort) extends Var {
  assert(id.matches("^[a-zA-Z0-9_\\-]*$"), id + " is an invalid identifier")
}

case class OtherClientLocalVar(id: String, sort: Sort) extends Var {
  def bind(c: Int) = GlobalVar(ClientNameMaker.make(id, c), sort)
}

case class ClientLocalVar(id: String, sort: Sort) extends Var {
  def bind(c: Int) = GlobalVar(ClientNameMaker.make(id, c), sort)
}

case class TransactionLocalVar(id: String, txn: String, sort: Sort) extends Var

case class UniqueTransactionLocalVar(id: String, txn: String, sort: Sort) extends Var

object FreeVar {

  var count: Int = 0

  def apply(sort: Sort): FreeVar = {
    count = count + 1
    FreeVar(count, sort)
  }

}

case class FreeVar(count: Int, sort: Sort) extends Var

@Salat
sealed trait Operation {

  /**
    * Warning: This is an asymmetric relation!
    */
  def commutesWith(other: Operation)(implicit g: AbstractHistory, param: Parameters): Expr = {
    import ExprBuilder._
    val symmetric = {
      val spec =
        if (param.pointWiseCommutativity) {
          g.system.pointWiseCommutativitySpec
        } else {
          g.system.commutativitySpecs
        }
      if (this.isQuery) {
        if (other.isQuery) {
          True
        } else {
          spec(other.id + "," + this.id).swap
        }
      } else {
        if (other.isQuery || this.id <= other.id) {
          spec(this.id + "," + other.id)
        } else {
          spec(other.id + "," + this.id).swap
        }
      }
    }
    if (param.encodeAsymmetricCommutativity) {
      g.system.asymmetricCommutativity.get(this.id + "," + other.id) match {
        case Some(x) => symmetric or x
        case None => symmetric
      }
    } else symmetric
  }

  val id: OperationID

  def isQuery: Boolean

  def isUpdate: Boolean
}

final case class Update(id: OperationID) extends Operation {
  def isQuery = false

  def isUpdate = true
}

final case class Query(id: OperationID) extends Operation {
  def isQuery = true

  def isUpdate = false
}

object Skip extends Operation {
  val id = "skip"

  def isQuery = false

  def isUpdate = false
}

final case class Event(
    id: EventID,
    txn: TransactionID,
    operationID: OperationID,
    constraint: Expr = True,
    displayCode: Boolean = false
) {

  assert(id.matches("^[A-Za-z0-9~!@$%^&*_\\-+=>\\.?/]+$"))
  assert(txn.matches("^[A-Za-z0-9~!@$%^&*_\\-+=>\\.?/]+$"))

  def instantiateToClient(c: Int): Event = {
    this.copy(
      id = ClientNameMaker.make(id, c),
      txn = ClientNameMaker.make(txn, c),
      constraint = constraint.instantiateToClient(c)
    )
  }

  /**
    * Warning: This is an asymmetric relation!
    */
  def commutesWith(other: Event)(implicit g: AbstractHistory, param: Parameters): Expr = {
    if (operationID == Skip.id || other.operationID == Skip.id) return True
    val left = g.operationMap(this.operationID)
    val right = g.operationMap(other.operationID)
    (left commutesWith right).instantiate(this, other)
  }

  def absorbedBy(other: Event, query: Option[Event] = None)(implicit g: AbstractHistory, param: Parameters): Expr = {
    import ExprBuilder._
    if (operationID == Skip.id) return True
    if (other.operationID == Skip.id) return False
    val left = g.operationMap(this.operationID)
    val right = g.operationMap(other.operationID)
    val binary: Expr = g.system.absorptionSpecs(left.id + "," + right.id).instantiate(this, other)
    if (param.encodeQuerySpecificAbsorption) {
      val ternary: Expr =
        query.flatMap { qE =>
          val op = g.operationMap(qE.operationID)
          g.system.querySpecificAbsorption.get(left.id + "," + right.id + "," + op.id)
            .map(x => x.instantiate(this, other, qE))
        }.getOrElse(False)
      binary or ternary
    } else {
      binary
    }
  }

  def operation(implicit g: AbstractHistory): Operation = {
    g.operationMap(this.operationID)
  }

}

final case class ToEdge(
    sourceID: TransactionID,
    targetID: TransactionID,
    isSynchronized: Boolean = false
) {

  def toTuple: (TransactionID, TransactionID) = (sourceID, targetID)

  @inline
  def contains(id: String): Boolean =
    sourceID == id || targetID == id

  def instantiateToClient(c: Int): ToEdge = {
    this.copy(
      sourceID = ClientNameMaker.make(sourceID, c),
      targetID = ClientNameMaker.make(targetID, c)
    )
  }

  def stripClient: ToEdge = {
    this.copy(sourceID =
      ClientNameMaker.deconstruct(this.sourceID)._1, targetID = ClientNameMaker.deconstruct(this.targetID)._1
    )
  }

}

final case class PoEdge(
    sourceID: EventID,
    targetID: EventID,
    constraint: Expr = True
) {

  @inline
  def contains(id: String): Boolean =
    sourceID == id || targetID == id

  def instantiateToClient(c: Int): PoEdge = {
    this.copy(
      sourceID = ClientNameMaker.make(sourceID, c),
      targetID = ClientNameMaker.make(targetID, c),
      constraint = constraint.instantiateToClient(c)
    )
  }

  @inline
  def source(implicit g: AbstractHistory): Event = g.eventMap(sourceID)

  @inline
  def target(implicit g: AbstractHistory): Event = g.eventMap(targetID)

  @inline
  def txn(implicit g: AbstractHistory): TransactionID = source.txn

}