/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package ch.ethz.inf.pm.cfour.output

import java.io.{File, FileWriter, PrintWriter}
import java.nio.file.Files

import ch.ethz.inf.pm.cfour.Parameters

/**
  * Export to the given path
  */
object FileSystemExporter {

  lazy val exportPath: String = Files.createTempDirectory("cfour").toFile.getAbsolutePath

  def load(fileName: String)(implicit param: Parameters): List[String] = {

    val path = param.exportDirectory.getOrElse(exportPath)
    val file = new File(path + File.separator + fileName.replaceAll("[^\\w\\.]+", ""))
    val source = scala.io.Source.fromFile(file)
    try source.getLines.toList finally source.close()

  }

  def export(fileName: String, contents: String)(implicit param: Parameters): String = {

    val path = param.exportDirectory.getOrElse(exportPath)

    val dir = new File(path)

    if (dir.isDirectory || dir.mkdir()) {

      val file = new File(path + File.separator + fileName.replaceAll("[^\\w\\.]+", ""))
      var fw: FileWriter = null
      var pw: PrintWriter = null

      try {

        fw = new FileWriter(file, false)
        pw = new PrintWriter(fw)

        pw.println(contents)

      } finally {

        if (pw != null) pw.close()
        if (fw != null) fw.close()

      }

      file.getAbsolutePath

    } else {
      throw new RuntimeException("Failed to create output directory " + dir)
    }

  }
}
