/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package ch.ethz.inf.pm.cfour.output

object WeightedGraph {

  case class Default[Node, Weight]() extends WeightedGraph[Node, Weight]

}

/**
  * This class represents an oriented weighted graph.
  *
  * @author Pietro Ferrara, Lucas Brutschy
  */
trait WeightedGraph[Node, Weight] {

  val nodeMapping = new scala.collection.mutable.HashMap[Node, Int]()
  var nodes: List[Node] = Nil
  var edges: Set[(Int, Int, Option[Weight])] = Set.empty

  def removeNode(node: Node) {
    val index: Int = this.addNodeIfNotExisting(node)
    var newEdges: Set[(Int, Int, Option[Weight])] = Set.empty
    for ((i1, i2, w) <- edges) {
      val index1 = if (i1 > index) i1 - 1 else i1
      val index2 = if (i2 > index) i2 - 1 else i2
      if (index != i1 && index != i2)
        newEdges = newEdges + ((index1, index2, w))
    }
    edges = newEdges
    nodes = remove(nodes, index)
  }

  /**
    * Add a node to the current graph iff it is not yet in the graph. Otherwise, it returns the index
    * of the existing node.
    *
    * @param node the node to be added if it does not exist
    * @return the index of the(eventually inserted) node
    */
  def addNodeIfNotExisting(node: Node): Int = {
    for (i <- nodes.indices)
      if (nodes.apply(i).equals(node)) return i
    nodes = nodes ::: node :: Nil
    nodes.lastIndexOf(node)
  }

  /**
    * Replace a node with the given one
    *
    * @param index the index of the node to be replaced
    * @param  node the node to be inserted
    */
  def setNode(index: Int, node: Node) {
    nodes = nodes.updated(index, node)
  }

  /**
    * Return the node ids without sorting edges
    *
    * @return the leaves of the graph
    */
  def getLeaves: Set[Node] = {
    getLeavesIds map { id => nodes(id) }
  }

  def getLeavesIds: Set[Int] = {
    var notLeafs: Set[Int] = Set.empty
    for ((i1, _, _) <- edges) //{
      notLeafs = notLeafs + i1
    var result: Set[Int] = Set.empty
    for (i <- nodes.indices)
      if (!notLeafs.contains(i))
        result = result + i
    result
  }

  def initialBlockInLoop(index: Int): Boolean = {
    if (entryEdges(index).size <= 1) return false
    var prevEdges: Set[(Int, Int, Option[Weight])] = this.exitEdges(index)
    var nextEdges: Set[(Int, Int, Option[Weight])] = prevEdges

    while (nextEdges.nonEmpty) {
      prevEdges = nextEdges
      nextEdges = Set.empty
      for ((i1, i2, _) <- prevEdges) {
        if (i2 > i1)
          nextEdges = nextEdges ++ this.exitEdges(i2)
        if (i2 == index)
          return true
      }
    }
    false
  }

  override def toString: String = {
    var result: String = ""
    var i: Int = 0
    while (i < nodes.size) {
      val node: Node = nodes.apply(i)
      result = result +
        "Node n." + i + "\n-----------------\n" +
        "Predecessor: " + entryNodesToString(i) +
        "\nSuccessor: " + exitNodesToString(i) +
        "\nBlock:\n" + nodeToString(node) + "\n-----------------\n"
      i = i + 1
    }
    result
  }

  protected def nodeToString(node: Node): String = node.toString

  def entryNodesToString(i: Int): String = {
    val preds = for ((from, _, weight) <- entryEdges(i))
      yield from + weightToString(weight)

    preds mkString ", "
  }

  def exitNodesToString(i: Int): String = {
    val succ = for ((_, to, weight) <- exitEdges(i))
      yield to + weightToString(weight)

    succ mkString ", "
  }

  def exitEdges(nodeIndex: Int): Set[(Int, Int, Option[Weight])] = {
    for (e@(from, _, _) <- edges if from == nodeIndex) yield e
  }

  private def weightToString(weight: Option[Weight]): String = weight match {
    case None => ""
    case null => ""
    case Some(x) => "(" + x.toString + ")"
  }

  /**
    * Add a node to the current graph
    *
    * @param node the node to be added
    * @return the index of the inserted node
    */
  def addNode(node: Node): Int = {
    nodes = nodes ::: node :: Nil
    val i = nodes.lastIndexOf(node)
    nodeMapping += (node -> i)
    i
  }

  /**
    * Add an edge to the current graph
    *
    * @param from   the node from which the edge starts
    * @param to     the node to which point the edge
    * @param weight the weight of the edge or None if this edge is not linked to a weight
    */
  def addEdge(from: Int, to: Int, weight: Option[Weight]): Unit = edges = edges.+((from, to, weight))

  /**
    * Add an edge to the current graph
    *
    * @param from   the node from which the edge starts
    * @param to     the node to which point the edge
    * @param weight the weight of the edge or None if this edge is not linked to a weight
    */
  def addEdge(from: Node, to: Node, weight: Option[Weight]): Unit = edges = edges.+((nodeMapping(from), nodeMapping(to), weight))

  def getDirectSuccessors(nodeIndex: Int): Set[Int] = {
    for ((_, to, _) <- exitEdges(nodeIndex)) yield to
  }

  def getDirectPredecessors(nodeIndex: Int): Set[Int] = {
    for ((from, _, _) <- entryEdges(nodeIndex)) yield from
  }

  def entryEdges(nodeIndex: Int): Set[(Int, Int, Option[Weight])] = {
    for (e@(_, to, _) <- edges if to == nodeIndex) yield e
  }

  private def remove[T1](list: List[T1], index: Int): List[T1] = list match {
    case x :: x1 if index != 0 => x :: remove(x1, index - 1)
    case _ :: x1 if index == 0 => remove(x1, index - 1)
    case Nil => Nil
  }

  def clearEdges() {
    edges = Set.empty
  }

  def filterEdges(f: (Node, Node, Option[Weight]) => Boolean) {
    edges = edges.filter { x => f(nodes(x._1), nodes(x._2), x._3) }
  }

}


case class LabeledGraph[Node, Weight]() extends WeightedGraph[Node, Weight] {

  private var extraInformation: List[String] = Nil
  private var nodeClasses = Map.empty[Node, String]
  private var edgeClasses = Map.empty[(Node, Node, Option[Weight]), String]
  private var nodeLabels = Map.empty[Node, String]
  private var edgeLabels = Map.empty[Weight, String]
  private var partitioning = Map.empty[Node, Node]

  def partitions: Set[Node] = partitioning.values.toSet

  def setNodeLabel(a: Node, b: String): Unit = {
    nodeLabels = nodeLabels + (a -> b)
  }

  def getNodeLabel(a: Node): String = {
    nodeLabels.getOrElse(a, "")
  }

  def setEdgeLabel(a: Weight, b: String): Unit = {
    edgeLabels = edgeLabels + (a -> b)
  }

  def getEdgeLabel(a: Weight): String = {
    edgeLabels.getOrElse(a, a.toString)
  }

  def setNodeClass(a: Node, b: String): Unit = {
    nodeClasses = nodeClasses + (a -> b)
  }

  def getNodeClass(a: Node): String = {
    nodeClasses.getOrElse(a, "")
  }

  def setEdgeClass(a: (Node, Node, Option[Weight]), b: String): Unit = {
    edgeClasses = edgeClasses + (a -> b)
  }

  def getEdgeClass(a: (Node, Node, Option[Weight])): String = {
    edgeClasses.getOrElse(a, "")
  }

  def addPartitioning(value: Node, target: Node): Unit =
    partitioning = partitioning + (value -> target)

  def getPartition(value: Node): Option[Node] =
    partitioning.get(value)

  def addExtraInformation(s: String): Unit =
    extraInformation ::= s

  def getExtraInformation: String = extraInformation.sorted.mkString("\n")
}