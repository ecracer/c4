/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package ch.ethz.inf.pm.cfour.output

import ch.ethz.inf.pm.cfour.Parameters
import com.typesafe.scalalogging.LazyLogging

object Logger {

  var tabs = 0

}

/**
  * Logging with TSV backend, timing and indentation
  */
trait Logger extends LazyLogging {

  def log_debug(str: String, tsvVal: Option[String] = None)(implicit param: Parameters): Unit = {
    if (param.debug) {
      if (param.quiet) ()
      else if (param.outputTSV) tsvVal.foreach(x => print(x + "\t"))
      else if (param.outputViaPrintln) println(t + str)
      else logger.debug(t + str)
    }
  }

  def log[A](name: String, x: (() => A), logTimeToTSV: Boolean = false, terminatingTSVEntry: Boolean = false)(implicit param: Parameters): A = {
    var res: Option[A] = None
    val timer = Timer()
    try {
      log_info(s">> $name")
      Logger.tabs += 2
      res = Some(x())
    } catch {
      case ex: Throwable =>
        Logger.tabs -= 2
        log_info(s"<< EXCEPTION during $name (took $timer)")
        throw ex
    }
    Logger.tabs -= 2
    log_info(s"<< $name (took $timer)", if (logTimeToTSV) Some(s"$timer") else None)
    if (terminatingTSVEntry) println()
    res.get
  }

  def log_info(str: String, tsvVal: Option[String] = None)(implicit param: Parameters): Unit =
    if (param.quiet) ()
    else if (param.outputTSV) tsvVal.foreach(x => print(x + "\t"))
    else if (param.outputViaPrintln) println(t + str)
    else logger.info(t + str)

  private def t = " " * Logger.tabs


}

/**
  * Used for timing
  *
  * @author Lucas Brutschy
  */
case class Timer(implicit param: Parameters) {
  val begin: Long = System.currentTimeMillis
  var end: Long = 0L

  override def toString: String = {
    end = System.currentTimeMillis
    ("%1.3f" format ((end - begin) / 1000.0)) + (if (!param.outputTSV) "s" else "")
  }
}
