package ch.ethz.inf.pm.cfour.output

import scala.io.Source

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

object ClassificationMap {

  type ClassificationMap = Map[String, Classification]
  val possibleClassifications = List(Error, Warning, Harmless, FalseAlarm, Unclassified)

  def loadFromFile(file: String): ClassificationMap = {
    val source = scala.io.Source.fromFile(file)
    loadFromSource(source)
  }

  def loadFromSource(source: Source): ClassificationMap = {
    try {
      val lines: List[String] = source.getLines.toList.map(_.trim).filterNot(_.startsWith("#")).filter(_.nonEmpty)
      val stringMap = lines.map(_.split("=") match { case Array(a, b) => (a, b) }).toMap
      stringMap.mapValues {
        case "h" => Harmless
        case "w" => Warning
        case "e" => Error
        case "n" => FalseAlarm
        case _ => Unclassified
      }
    } finally source.close()
  }

  sealed trait Classification

  case object Error extends Classification

  case object Warning extends Classification

  case object Harmless extends Classification

  case object FalseAlarm extends Classification

  case object Unclassified extends Classification

}

