/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package ch.ethz.inf.pm.cfour.real

import java.io.File

import ch.ethz.inf.pm.cfour.Parameters.testParameters
import ch.ethz.inf.pm.cfour.Unfolder.ViolationSet
import ch.ethz.inf.pm.cfour.output.{ClassificationMap, DumpGraph, Logger, Statistics}
import ch.ethz.inf.pm.cfour.{Parameters, TestSettings, Unfolder}
import org.scalatest.{BeforeAndAfter, FunSuite}

class CassandraFilteredTest extends FunSuite with TestSettings with BeforeAndAfter with Logger {

  test("cassandra-lock") {
    assert(analyze("cassandra-lock").size == 0)
  }

  test("cassandra-twitter") {
    assert(analyze("cassandra-twitter").size == 2)
  }

  test("cassatwitter") {
    assert(analyze("cassatwitter").size == 2)
  }

  test("cassieq-core") {
    assert(analyze("cassieq-core").size == 3)
  }

  test("currency-exchange") {
    assert(analyze("currency-exchange").size == 0)
  }

  test("datastax-queueing") {
    assert(analyze("datastax-queueing").size == 2)
  }

  test("killrchat") {
    assert(analyze("killrchat").size == 4)
  }

  test("playlist") {
    assert(analyze("playlist").size == 2)
  }

  test("roomstore") {
    assert(analyze("roomstore").size == 0)
  }

  test("shopping-cart") {
    assert(analyze("shopping-cart").size == 0)
  }

  test("twissandra") {
    assert(analyze("twissandra").size == 1)
  }

  def analyze(name: String): ViolationSet = {

    val classification = ClassificationMap.loadFromSource(getResource(s"cassandra/$name-violations.txt"))
    implicit val param: Parameters =
      Parameters.setCassandraFiltered(Parameters.testParameters).copy(
        exportDirectory = testParameters.exportDirectory.map(_ + File.separator + name),
        classificationMap = Some(classification),
        debug = false,
        resultGeneralizationCheck = true
      )
    Parameters.set(param)
    val inputGraph = getAbstractHistoryFromResource("cassandra", s"$name-filtered")
    log_info(" ==== Analyzing abstract event graph: " + DumpGraph.dumpToFile("input" + name, inputGraph.toLabeledGraph), Some(name))
    val res = Unfolder.unfoldAndCheck(inputGraph)

    // Check that generalization check passes
    assert(res.generalizesToArbitraryClients)

    // Check that all violations are classified
    assert(Statistics.classificationMap.getOrElse(ClassificationMap.Unclassified, Set.empty).isEmpty)

    // Check that no errors are filtered
    for (map <- param.classificationMap) assert((map.filter(_._2 == ClassificationMap.Error).keySet -- res.set.map(_.name)).isEmpty)

    res

  }

}
