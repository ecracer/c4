/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package ch.ethz.inf.pm.cfour

import ch.ethz.inf.pm.cfour.TestPrograms._
import ch.ethz.inf.pm.cfour.output.Statistics
import org.scalatest.{BeforeAndAfter, FunSuite, Matchers}

class TerminationCheckTest extends FunSuite with Matchers with TestSettings with BeforeAndAfter {

  before {
    Statistics.reset()
  }

  test("Three roles problem") {

    assert(!check(threeRolesProblem))

  }

  test("Single dekker client") {

    assert(check(singleDekkerClient))

  }

  test("Path sensitive 4 violation") {

    assert(check(pathSensitive4Violation))

  }

  test("Path sensitive no violation") {

    assert(check(pathSensitiveNoViolation))

  }

  test("Path sensitive violation") {

    assert(check(pathSensitiveViolation))

  }

  def check(g: AbstractHistory): Boolean = {
    Unfolder.unfoldAndCheck(g)(Parameters.testParameters.copy(resultGeneralizationCheck = true)).generalizesToArbitraryClients
  }
}
