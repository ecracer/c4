/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package ch.ethz.inf.pm.cfour.real

import java.io.File

import ch.ethz.inf.pm.cfour.Parameters.testParameters
import ch.ethz.inf.pm.cfour.Unfolder.ViolationSet
import ch.ethz.inf.pm.cfour.output.{ClassificationMap, DumpGraph, Logger, Statistics}
import ch.ethz.inf.pm.cfour.{Parameters, TestSettings, Unfolder}
import org.scalatest.{BeforeAndAfter, FunSuite}

class TouchDevelopUnfilteredTest extends FunSuite with TestSettings with BeforeAndAfter with Logger {

  test("blqz") {
    assert(analyze("blqz").size == 3)
  }
  test("cvuz") {
    assert(analyze("cvuz").size == 7)
  }
  test("eddm") {
    assert(analyze("eddm").size == 13)
  }
  test("eijba") {
    assert(analyze("eijba").size == 1)
  }
  test("etww") {
    assert(analyze("etww").size == 1)
  }
  test("fqaba") {
    assert(analyze("fqaba").size == 9)
  }
  test("gcane") {
    assert(analyze("gcane").size == 3)
  }
  test("kjxzcgcv") {
    assert(analyze("kjxzcgcv").size == 8)
  }
  test("kmac") {
    assert(analyze("kmac").size == 0)
  }
  test("nggfa") {
    assert(analyze("nggfa").size == 2)
  }
  test("nvoha") {
    assert(analyze("nvoha").size == 2)
  }
  test("padg") {
    assert(analyze("padg").size == 35)
  }
  test("qwidc") {
    assert(analyze("qwidc").size == 2)
  }
  test("qzju") {
    assert(analyze("qzju").size == 6)
  }
  test("ruef") {
    assert(analyze("ruef").size == 19)
  }
  test("ulvma") {
    assert(analyze("ulvma").size == 3)
  }
  test("wbuei") {
    assert(analyze("wbuei").size == 4)
  }

  def analyze(name: String): ViolationSet = {

    val classification = ClassificationMap.loadFromSource(getResource(s"touchdevelop/$name.txt"))
    implicit val param: Parameters =
      Parameters.setTouchDevelopUnfiltered(Parameters.testParameters).copy(
        exportDirectory = testParameters.exportDirectory.map(_ + File.separator + name),
        classificationMap = Some(classification),
        debug = false,
        resultGeneralizationCheck = true
      )
    val inputGraph = getAbstractHistoryFromResource("touchdevelop", s"$name.model")
    log_info(" ==== Analyzing abstract event graph: " + DumpGraph.dumpToFile("input" + name, inputGraph.toLabeledGraph), Some(name))
    val res = Unfolder.unfoldAndCheck(inputGraph)

    // Check that generalization check passes
    assert(res.generalizesToArbitraryClients)

    // Check that all violations are classified
    assert(Statistics.classificationMap.getOrElse(ClassificationMap.Unclassified, Set.empty).isEmpty)

    res

  }

}
