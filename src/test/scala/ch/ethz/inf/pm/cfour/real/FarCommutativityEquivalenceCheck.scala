/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package ch.ethz.inf.pm.cfour.real

import ch.ethz.inf.pm.cfour.Unfolder.ViolationSet
import ch.ethz.inf.pm.cfour.output.Logger
import ch.ethz.inf.pm.cfour.{Parameters, TestSettings, Unfolder}
import org.scalatest.{BeforeAndAfter, FunSuite}

class FarCommutativityEquivalenceCheck extends FunSuite with TestSettings with BeforeAndAfter with Logger {

  implicit val param: Parameters = Parameters.testParameters.copy(
    onlyFarCommutativityEquivalenceCheck = true,
    farCommutativityEquivalenceCheck = true,
    debug = false,
    useShortNames = false,
    encodeAsymmetricCommutativity = false
  )

  test("blqz") {
    analyzeTD("blqz")
  }
  test("cvuz") {
    analyzeTD("cvuz")
  }
  test("eddm") {
    analyzeTD("eddm")
  }
  test("eijba") {
    analyzeTD("eijba")
  }
  test("etww") {
    analyzeTD("etww")
  }
  test("fqaba") {
    analyzeTD("fqaba")
  }
  test("gcane") {
    analyzeTD("gcane")
  }
  test("kmac") {
    analyzeTD("kmac")
  }
  test("nggfa") {
    analyzeTD("nggfa")
  }
  test("nvoha") {
    analyzeTD("nvoha")
  }
  test("padg") {
    analyzeTD("padg")
  }
  test("qwidc") {
    analyzeTD("qwidc")
  }
  test("qzju") {
    analyzeTD("qzju")
  }
  test("ruef") {
    analyzeTD("ruef")
  }
  test("ulvma") {
    analyzeTD("ulvma")
  }
  test("wbuei") {
    analyzeTD("wbuei")
  }

  test("cassandra-lock") {
    analyzeCass("cassandra-lock")
  }
  test("cassandra-twitter") {
    analyzeCass("cassandra-twitter")
  }
  test("cassatwitter") {
    analyzeCass("cassatwitter")
  }
  test("cassieq-core") {
    analyzeCass("cassieq-core")
  }
  test("currency-exchange") {
    analyzeCass("currency-exchange")
  }
  test("datastax-queueing") {
    analyzeCass("datastax-queueing")
  }
  test("killrchat") {
    analyzeCass("killrchat")
  }
  test("playlist") {
    analyzeCass("playlist")
  }
  test("roomstore") {
    analyzeCass("roomstore")
  }
  test("shopping-cart") {
    analyzeCass("shopping-cart")
  }
  test("simple-twitter") {
    analyzeCass("simple-twitter")
  }
  test("twissandra") {
    analyzeCass("twissandra")
  }

  def analyzeTD(name: String): ViolationSet = {
    val inputGraph = getAbstractHistoryFromResource("touchdevelop", s"$name.model")
    val res = Unfolder.unfoldAndCheck(inputGraph)
    println("    (exception expected!)")
    res
  }

  def analyzeCass(name: String): ViolationSet = {
    val inputGraph = getAbstractHistoryFromResource("cassandra", s"$name-unfiltered")
    val res = Unfolder.unfoldAndCheck(inputGraph)
    println("    (exception expected!)")
    res
  }

}
