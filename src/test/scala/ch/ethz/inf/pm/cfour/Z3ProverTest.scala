/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package ch.ethz.inf.pm.cfour

import ch.ethz.inf.pm.cfour.output.Logger
import org.scalatest.FunSuite

class Z3ProverTest extends FunSuite with TestSettings with Logger {

  implicit val param: Parameters = Parameters.testParameters

  test("Version") {
    Z3Prover.withZ3({ z3: Z3Prover[Expr, Var] =>
      log_info(z3.z3Version().toString)
    }, EventGraphZ3Converter(Parameters.testParameters))
  }

  test("Direct from string") {
    Z3Prover.withZ3({ z3: Z3Prover[Expr, Var] =>
      z3.emit("(declare-const a Int)")
      z3.emit("(assert (> a 10))")
      assert(z3.check(Some(100)) == Z3Prover.Sat)
    }, EventGraphZ3Converter(Parameters.testParameters))
  }

  test("From expression builder") {
    Z3Prover.withZ3({ z3: Z3Prover[Expr, Var] =>
      val a = GlobalVar("XX", IntSort)
      z3.assume(Equal(a, IntConst(10)))
      assert(z3.check(Some(100)) == Z3Prover.Sat)
    }, EventGraphZ3Converter(Parameters.testParameters))
  }

}
