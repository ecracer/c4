/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package ch.ethz.inf.pm.cfour

import ch.ethz.inf.pm.cfour.ExprBuilder._

object TestPrograms {

  val threeRolesProblem =
    AbstractHistory(
      events = List(
        Event("entry", "t", "skip"),
        Event("u1", "t", "put", ArgLeft(0, StringSort) equal StringConst("1")),
        Event("q1", "t", "get", ArgLeft(0, StringSort) equal StringConst("2")),
        Event("u2", "t", "put", ArgLeft(0, StringSort) equal StringConst("2")),
        Event("q2", "t", "get", ArgLeft(0, StringSort) equal StringConst("3")),
        Event("u3", "t", "put", ArgLeft(0, StringSort) equal StringConst("3")),
        Event("q3", "t", "get", ArgLeft(0, StringSort) equal StringConst("1")),
        Event("exit", "t", "skip")
      ),
      programOrder = List(
        PoEdge("entry", "u1", ClientLocalVar("user", StringSort) equal StringConst("user1")),
        PoEdge("entry", "u2", ClientLocalVar("user", StringSort) equal StringConst("user2")),
        PoEdge("entry", "u3", ClientLocalVar("user", StringSort) equal StringConst("user3")),
        PoEdge("u1", "q1"),
        PoEdge("u2", "q2"),
        PoEdge("u3", "q3"),
        PoEdge("q1", "exit"),
        PoEdge("q2", "exit"),
        PoEdge("q3", "exit")
      ),
      transactionOrder = List(ToEdge("t", "t")),
      system = SystemSpecification.PutGetIncClearMap
    )


  val singleDekkerClientWithEntryExit =
    AbstractHistory(
      events = List(
        Event("u", "u", "put"),
        Event("q", "q", "get"),
        Event("entry_u", "u", "skip"),
        Event("entry_q", "q", "skip"),
        Event("exit_u", "u", "skip"),
        Event("exit_q", "q", "skip")
      ),
      transactionOrder = List(
        ToEdge("u", "q")
      ),
      programOrder = List(
        PoEdge("entry_u", "u"),
        PoEdge("u", "exit_u"),
        PoEdge("entry_q", "q"),
        PoEdge("q", "exit_q")
      ),
      system = SystemSpecification.PutGetIncClearMap
    )


  val singleDekkerClient =
    AbstractHistory(
      events = List(
        Event("u", "u", "put"),
        Event("q", "q", "get")
      ),
      transactionOrder = List(
        ToEdge("u", "q")
      ),
      programOrder = Nil,
      system = SystemSpecification.PutGetIncClearMap
    )

  val incPutGet =
    AbstractHistory(
      events = List(
        Event("inc", "inc", "inc"),
        Event("put", "put", "put"),
        Event("get", "get", "get")
      ),
      programOrder = Nil,
      transactionOrder = List(
        ToEdge("inc", "get"),
        ToEdge("put", "get")
      ),
      system = SystemSpecification.PutGetIncClearMap
    )

  val withTrivialTransactions =
    AbstractHistory(
      events = List(
        Event("u1", "t1", "put"),
        Event("q1", "t1", "get"),
        Event("u2", "t2", "skip"),
        Event("q2", "t2", "skip")
      ),
      programOrder = List(
        PoEdge("u1", "q1"),
        PoEdge("u2", "q2")
      ),
      transactionOrder = Nil,
      system = SystemSpecification.PutGetIncClearMap
    )

  val dekkerSingleTransaction =
    AbstractHistory(
      events = List(
        Event("u1", "t1", "put"),
        Event("q1", "t1", "get"),
        Event("u2", "t2", "put"),
        Event("q2", "t2", "get")
      ),
      programOrder = List(
        PoEdge("u1", "q1"),
        PoEdge("u2", "q2")
      ),
      transactionOrder = Nil,
      system = SystemSpecification.PutGetIncClearMap
    )

  val singleLocalStoreClient =
    AbstractHistory(
      events = List(
        Event("u", "u", "put", ArgLeft(0, StringSort) equal ClientLocalVar("username", StringSort)),
        Event("q", "q", "get", ArgLeft(0, StringSort) equal ClientLocalVar("username", StringSort))
      ),
      transactionOrder = List(
        ToEdge("u", "q")
      ),
      programOrder = Nil,
      system = SystemSpecification.PutGetIncClearMap
    )

  val singleLocalStoreOneTransaction =
    AbstractHistory(
      events = List(
        Event("u", "t", "put", ArgLeft(0, StringSort) equal ClientLocalVar("username", StringSort)),
        Event("q", "t", "get", ArgLeft(0, StringSort) equal ClientLocalVar("username", StringSort))
      ),
      transactionOrder = Nil,
      programOrder = List(
        PoEdge("u", "q")
      ),
      system = SystemSpecification.PutGetIncClearMap
    )

  val allSessionOnSameValue =
    AbstractHistory(
      events = List(
        Event("u", "u", "put", ArgLeft(0, StringSort) equal StringConst("a")),
        Event("q", "q", "get", ArgLeft(0, StringSort) equal StringConst("a"))
      ),
      transactionOrder = List(
        ToEdge("u", "q")
      ),
      programOrder = Nil,
      system = SystemSpecification.PutGetIncClearMap
    )

  val singleDekkerClientWithClientLocalConstraint =
    AbstractHistory(
      events = List(
        Event("u", "u", "put", ArgLeft(0, StringSort) equal ClientLocalVar("uuid1", StringSort)),
        Event("q", "q", "get", ArgLeft(0, StringSort) equal ClientLocalVar("uuid2", StringSort))
      ),
      transactionOrder = List(
        ToEdge("u", "q")
      ),
      programOrder = Nil,
      system = SystemSpecification.PutGetIncClearMap,
      globalConstraint = And(
        Unequal(ClientLocalVar("uuid1", StringSort), OtherClientLocalVar("uuid2", StringSort)),
        Unequal(ClientLocalVar("uuid2", StringSort), OtherClientLocalVar("uuid2", StringSort))
      )
    )

  val loopInProgramOrder =
    AbstractHistory(
      events = List(
        Event("u1", "t1", "put"),
        Event("u2", "t1", "put"),
        Event("u3", "t1", "put"),
        Event("u4", "t2", "put")
      ),
      programOrder = List(
        PoEdge("u1", "u2"),
        PoEdge("u2", "u2"),
        PoEdge("u2", "u3")
      ),
      transactionOrder = Nil,
      system = SystemSpecification.PutGetIncClearMap
    )

  val complexLoopInProgramOrder =
    AbstractHistory(
      events = List(
        Event("entry", "t1", "skip"),
        Event("u1", "t1", "put"),
        Event("u2", "t1", "put"),
        Event("u3", "t1", "put"),
        Event("u4", "t1", "put"),
        Event("u5", "t1", "put"),
        Event("u6", "t1", "put"),
        Event("u7", "t1", "put"),
        Event("u8", "t1", "put"),
        Event("exit", "t1", "skip")
      ),
      programOrder = List(
        PoEdge("entry", "u1"),
        PoEdge("u1", "u2"),
        PoEdge("u2", "u1"),
        PoEdge("u2", "u3"),
        PoEdge("u3", "u4"),
        PoEdge("u4", "u5"),
        PoEdge("u4", "u1"),
        PoEdge("u5", "u6"),
        PoEdge("u6", "u7"),
        PoEdge("u7", "u8"),
        PoEdge("u8", "u8"),
        PoEdge("u8", "exit")
      ),
      transactionOrder = Nil,
      system = SystemSpecification.PutGetIncClearMap
    )

  val pathSensitiveNoViolation =
    AbstractHistory(
      events = List(
        Event("entry", "t", "skip"),
        Event("u", "t", "put"),
        Event("q", "t", "get"),
        Event("exit", "t", "skip")
      ),
      programOrder = List(
        PoEdge("entry", "u", ClientLocalVar("user", StringSort) equal StringConst("producer")),
        PoEdge("entry", "q", ClientLocalVar("user", StringSort) unequal StringConst("producer")),
        PoEdge("u", "exit"),
        PoEdge("q", "exit")
      ),
      transactionOrder = List(ToEdge("t", "t")),
      system = SystemSpecification.PutGetIncClearMap,
      globalConstraint =
        OtherClientLocalVar("user", StringSort) unequal ClientLocalVar("user", StringSort)
    )

  val pathSensitive4Violation =
    AbstractHistory(
      events = List(
        Event("entry", "t", "skip"),
        Event("u", "t", "inc"),
        Event("q", "t", "get"),
        Event("exit", "t", "skip")
      ),
      programOrder = List(
        PoEdge("entry", "u", ClientLocalVar("user", StringSort) equal StringConst("producer")),
        PoEdge("entry", "q", ClientLocalVar("user", StringSort) unequal StringConst("producer")),
        PoEdge("u", "exit"),
        PoEdge("q", "exit")
      ),
      transactionOrder = List(ToEdge("t", "t")),
      system = SystemSpecification.PutGetIncClearMap,
      globalConstraint =
        OtherClientLocalVar("user", StringSort) unequal ClientLocalVar("user", StringSort)
    )

  val pathSensitiveViolation =
    AbstractHistory(
      events = List(
        Event("entry", "t", "skip"),
        Event("u", "t", "put"),
        Event("q", "t", "get"),
        Event("exit", "t", "skip")
      ),
      programOrder = List(
        PoEdge("entry", "u"),
        PoEdge("entry", "q"),
        PoEdge("u", "exit"),
        PoEdge("q", "exit")
      ),
      transactionOrder = List(ToEdge("t", "t")),
      system = SystemSpecification.PutGetIncClearMap,
      globalConstraint =
        OtherClientLocalVar("user", StringSort) unequal ClientLocalVar("user", StringSort)
    )

  val withLoop =
    AbstractHistory(
      events = List(
        Event("u1", "t1", "put")
      ),
      programOrder = Nil,
      transactionOrder = List(
        ToEdge("t1", "t1")
      ),
      system = SystemSpecification.PutGetIncClearMap
    )

  val whileTrueLoop =
    AbstractHistory(
      events = List(
        Event("e1", "t1", "skip"),
        Event("e2", "t1", "put"),
        Event("e3", "t1", "get"),
        Event("e4", "t1", "skip")
      ),
      programOrder = List(
        PoEdge("e1", "e2"),
        PoEdge("e2", "e3"),
        PoEdge("e3", "e2"),
        PoEdge("e2", "e4")
      ),
      transactionOrder = Nil,
      system = SystemSpecification.PutGetIncClearMap
    )

  val fullyConnected: AbstractHistory = {
    val events = Set("e1", "e2", "e3", "e4", "e5", "e6")
    AbstractHistory(
      events = List(
        Event("e1", "t1", "skip"),
        Event("e2", "t1", "put"),
        Event("e3", "t1", "get"),
        Event("e4", "t1", "put"),
        Event("e5", "t1", "get"),
        Event("e6", "t1", "skip")
      ),
      programOrder =
        (for (a <- events; b <- events if a != b && a != "e6" && b != "e1") yield {
          PoEdge(a, b)
        }).toList,
      transactionOrder = Nil,
      system = SystemSpecification.PutGetIncClearMap
    )
  }

  val playlistLoginServletPst: AbstractHistory = {
    AbstractHistory(
      events = List(
        Event("entry", "t1", "skip"),
        Event("q1", "t1", "get", Equal(ArgLeft(0, StringSort), ClientLocalVar("q1", StringSort))),
        Event("u1", "t1", "put", Equal(ArgLeft(0, StringSort), ClientLocalVar("u1", StringSort))),
        Event("uc1", "t1", "put", Equal(ArgLeft(0, StringSort), ClientLocalVar("uc1", StringSort))),
        Event("uc2", "t1", "put", Equal(ArgLeft(0, StringSort), ClientLocalVar("uc2", StringSort))),
        Event("uc3", "t1", "put", Equal(ArgLeft(0, StringSort), ClientLocalVar("uc3", StringSort))),
        Event("exit", "t1", "skip")
      ),
      programOrder = List(
        PoEdge("entry", "uc1"),
        PoEdge("entry", "q1"),
        PoEdge("entry", "uc3"),
        PoEdge("q1", "uc1"),
        PoEdge("q1", "uc2"),
        PoEdge("uc3", "u1"),
        PoEdge("uc3", "exit"),
        PoEdge("uc1", "exit"),
        PoEdge("uc2", "exit"),
        PoEdge("u1", "exit")
      ),
      transactionOrder = Nil,
      system = SystemSpecification.PutGetIncClearMap,
      globalConstraint = And(Unequal(ClientLocalVar("q1", StringSort), ClientLocalVar("uc1", StringSort)),
        And(Unequal(ClientLocalVar("q1", StringSort), OtherClientLocalVar("uc1", StringSort)),
          And(Unequal(ClientLocalVar("q1", StringSort), ClientLocalVar("uc2", StringSort)),
            And(Unequal(ClientLocalVar("q1", StringSort), OtherClientLocalVar("uc2", StringSort)),
              And(Unequal(ClientLocalVar("q1", StringSort), ClientLocalVar("uc3", StringSort)),
                And(Unequal(ClientLocalVar("q1", StringSort), OtherClientLocalVar("uc3", StringSort)),
                  And(Unequal(ClientLocalVar("u1", StringSort), ClientLocalVar("uc1", StringSort)),
                    And(Unequal(ClientLocalVar("u1", StringSort), OtherClientLocalVar("uc1", StringSort)),
                      And(Unequal(ClientLocalVar("u1", StringSort), ClientLocalVar("uc2", StringSort)),
                        And(Unequal(ClientLocalVar("u1", StringSort), OtherClientLocalVar("uc2", StringSort)),
                          And(Unequal(ClientLocalVar("u1", StringSort), ClientLocalVar("uc3", StringSort)),
                            And(Unequal(ClientLocalVar("u1", StringSort), OtherClientLocalVar("uc3", StringSort)),
                              And(Unequal(ClientLocalVar("uc1", StringSort), OtherClientLocalVar("uc1", StringSort)),
                                And(Unequal(ClientLocalVar("uc1", StringSort), OtherClientLocalVar("uc2", StringSort)),
                                  And(Unequal(ClientLocalVar("uc1", StringSort), OtherClientLocalVar("uc3", StringSort)),
                                    And(Unequal(ClientLocalVar("uc2", StringSort), OtherClientLocalVar("uc2", StringSort)),
                                      And(Unequal(ClientLocalVar("uc2", StringSort), OtherClientLocalVar("uc3", StringSort)),
                                        Unequal(ClientLocalVar("uc3", StringSort), OtherClientLocalVar("uc3", StringSort))
                                      )))))))))))))))))
    )
  }

  val tableAddExists =
    AbstractHistory(
      events = List(
        Event("u", "u", "add"),
        Event("q", "q", "exists")
      ),
      transactionOrder = List(
        ToEdge("u", "q")
      ),
      programOrder = Nil,
      system = SystemSpecification.Table
    )
  val tableAddAll =
    AbstractHistory(
      events = List(
        Event("u", "u", "add"),
        Event("q", "q", "all")
      ),
      transactionOrder = List(
        ToEdge("u", "q")
      ),
      programOrder = Nil,
      system = SystemSpecification.Table
    )

}
