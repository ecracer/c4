/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package ch.ethz.inf.pm.cfour

import ch.ethz.inf.pm.cfour.Unfolder.ViolationSet
import ch.ethz.inf.pm.cfour.output.{DumpGraph, Logger}
import org.scalatest.{BeforeAndAfter, FunSuite}

class BugTest extends FunSuite with TestSettings with BeforeAndAfter with Logger {

  test("missing_violation") {
    assert(analyze("missing_violation").nonEmpty)
  }

  test("etww_bug") {
    analyze("etww_bug")
  }

  test("red_to_graph") {
    analyze("red_to_graph")
  }


  def analyze(name: String): ViolationSet = {

    implicit val param: Parameters = Parameters.testParameters
    val inputGraph = getAbstractHistoryFromResource("bugs", name)
    log_info(" ==== Analyzing abstract event graph: " + DumpGraph.dumpToFile("input" + name, inputGraph.toLabeledGraph), Some(name))
    Unfolder.unfoldAndCheck(inputGraph)

  }

}
