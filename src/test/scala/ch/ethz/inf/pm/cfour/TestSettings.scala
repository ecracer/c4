/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package ch.ethz.inf.pm.cfour

import ch.ethz.inf.pm.cfour.AbstractHistory.fromJSON

import scala.io.Source

trait TestSettings {

  def getAbstractHistoryFromResource(dir: String, name: String): AbstractHistory = {
    val json = getResource(s"$dir/$name.json").mkString.replace("\uFEFF", "") // UTF16 BOM
    fromJSON(json)

  }

  def getResource(path: String): Source = {
    val stream = getClass.getClassLoader.getResourceAsStream(path)
    io.Source.fromInputStream(stream)
  }

}
