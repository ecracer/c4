#!/usr/bin/env bash

declare -a touchdevelop=("blqz" "cvuz" "eddm" "eijba" "etww" "fqaba" "gcane" "kjxzcgcv" "kmac" "nggfa" "nvoha" "padg" "qwidc" "qzju" "ruef" "ulvma" "wbuei")
declare -a cassandra=("cassandra-lock" "cassandra-twitter" "cassatwitter" "cassieq-core" "currency-exchange" "datastax-queueing" "killrchat" "playlist" "roomstore" "shopping-cart" "twissandra")
declare -a files=("-harmful" "-harmless" "-displayCode" "-atomicSet")
declare -a backends=("touchdevelop" "cassandra")

CFOUR="java -jar cfour.jar"
OUTPUT_DIRECTORY=/tmp/cfour/filter-stats
INPUT_DIRECTORY=src/test/resources

mkdir -p ${OUTPUT_DIRECTORY}

# UNFILTERED
echo === TouchDevelop violations, unfiltered
rm -f ${OUTPUT_DIRECTORY}/violations-touchdevelop-unfiltered.txt
for i in "${touchdevelop[@]}"
do
    echo ${i}
    ${CFOUR} -dumpViolationIDs -touchdevelopUnfiltered -disableGenTest \
        -classificationFile=${INPUT_DIRECTORY}/touchdevelop/${i}.txt < ${INPUT_DIRECTORY}/touchdevelop/${i}.model.json \
            | grep . | sed -e "s/^/${i}:/" \
        >> ${OUTPUT_DIRECTORY}/violations-touchdevelop-unfiltered.txt
done
echo === Cassandra violations, unfiltered
rm -f ${OUTPUT_DIRECTORY}/violations-cassandra-unfiltered.txt
for i in "${cassandra[@]}"
do
    echo ${i}
    ${CFOUR} -dumpViolationIDs -cassandraUnfiltered -disableGenTest \
        -classificationFile=${INPUT_DIRECTORY}/cassandra/${i}-violations.txt < ${INPUT_DIRECTORY}/cassandra/${i}-unfiltered.json \
            | grep . | sed -e "s/^/${i}:/" \
        >> ${OUTPUT_DIRECTORY}/violations-cassandra-unfiltered.txt
done

# DISPLAY CODE
echo === TouchDevelop violations, no display code
rm -f ${OUTPUT_DIRECTORY}/violations-touchdevelop-displayCode.txt
for i in "${touchdevelop[@]}"
do
    echo ${i}
    ${CFOUR} -dumpViolationIDs -touchdevelopFiltered -disableAtomicSets -disableGenTest \
        -classificationFile=${INPUT_DIRECTORY}/touchdevelop/${i}.txt < ${INPUT_DIRECTORY}/touchdevelop/${i}.model.json \
            | grep . | sed -e "s/^/${i}:/" \
        >> ${OUTPUT_DIRECTORY}/violations-touchdevelop-displayCode.txt
done
echo === Cassandra violations, no display code
rm -f ${OUTPUT_DIRECTORY}/violations-cassandra-displayCode.txt
for i in "${cassandra[@]}"
do
    echo ${i}
    ${CFOUR} -dumpViolationIDs -cassandraFiltered -disableGenTest \
        -classificationFile=${INPUT_DIRECTORY}/cassandra/${i}-violations.txt < ${INPUT_DIRECTORY}/cassandra/${i}-filtered.json \
            | grep . | sed -e "s/^/${i}:/" \
        >> ${OUTPUT_DIRECTORY}/violations-cassandra-displayCode.txt
done

# ATOMIC SETS
echo === TouchDevelop violations, atomic sets
rm -f ${OUTPUT_DIRECTORY}/violations-touchdevelop-atomicSets.txt
for i in "${touchdevelop[@]}"
do
    echo ${i}
    ${CFOUR} -dumpViolationIDs -touchdevelopFiltered -disableDisplayCode -disableGenTest \
        -classificationFile=${INPUT_DIRECTORY}/touchdevelop/${i}.txt < ${INPUT_DIRECTORY}/touchdevelop/${i}.model.json \
            | grep . | sed -e "s/^/${i}:/" \
        >> ${OUTPUT_DIRECTORY}/violations-touchdevelop-atomicSets.txt
done
cp ${OUTPUT_DIRECTORY}/violations-cassandra-unfiltered.txt ${OUTPUT_DIRECTORY}/violations-cassandra-atomicSets.txt

# HARMFUL/HARMLESS/BASELINE
for b in "${backends[@]}"
do
    grep Error ${OUTPUT_DIRECTORY}/violations-${b}-unfiltered.txt > ${OUTPUT_DIRECTORY}/violations-${b}-venn-harmful.txt
    grep Harmless ${OUTPUT_DIRECTORY}/violations-${b}-unfiltered.txt > ${OUTPUT_DIRECTORY}/violations-${b}-venn-harmless.txt
    grep Warning ${OUTPUT_DIRECTORY}/violations-${b}-unfiltered.txt >> ${OUTPUT_DIRECTORY}/violations-${b}-venn-harmless.txt
    cat ${OUTPUT_DIRECTORY}/violations-${b}-venn-harmful.txt ${OUTPUT_DIRECTORY}/violations-${b}-venn-harmless.txt > ${OUTPUT_DIRECTORY}/violations-${b}-baseline.txt

    comm -23 \
        <(sort ${OUTPUT_DIRECTORY}/violations-${b}-baseline.txt) \
        <(sort ${OUTPUT_DIRECTORY}/violations-${b}-atomicSets.txt) \
        > ${OUTPUT_DIRECTORY}/violations-${b}-venn-atomicSets.txt

    comm -23 \
        <(sort ${OUTPUT_DIRECTORY}/violations-${b}-baseline.txt) \
        <(sort ${OUTPUT_DIRECTORY}/violations-${b}-displayCode.txt) \
        > ${OUTPUT_DIRECTORY}/violations-${b}-venn-displayCode.txt

done


for b in "${backends[@]}"
do
    echo "For ${b}, open http://bioinformatics.psb.ugent.be/webtools/Venn/ - upload files:"
    for o in "${files[@]:2}"
    do
        echo "${OUTPUT_DIRECTORY}/violations-${b}-venn${o}.txt"
    done
done


