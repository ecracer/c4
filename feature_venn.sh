#!/usr/bin/env bash

declare -a touchdevelop=("blqz" "cvuz" "eddm" "eijba" "etww" "fqaba" "gcane" "kjxzcgcv" "kmac" "nggfa" "nvoha" "padg" "qwidc" "qzju" "ruef" "ulvma" "wbuei")
declare -a cassandra=("cassandra-lock" "cassandra-twitter" "cassatwitter" "cassieq-core" "currency-exchange" "datastax-queueing" "killrchat" "playlist" "roomstore" "shopping-cart" "twissandra")
declare -a options=("-quickCheck" "" "-disableProcesses" "-disableConstraints" "-disableAbsorption" "-disableCommutativity")
declare -a backends=("touchdevelop" "cassandra")

CFOUR="java -jar cfour.jar"
OUTPUT_DIRECTORY=/tmp/cfour/feature-stats
INPUT_DIRECTORY=src/test/resources

mkdir -p ${OUTPUT_DIRECTORY}

for j in "${options[@]}"
do
    echo === TouchDevelop violations with option ${j}
    rm -f ${OUTPUT_DIRECTORY}/violations-touchdevelop${j}.txt
    for i in "${touchdevelop[@]}"
    do
        echo ${i}
        ${CFOUR} -dumpViolationIDs -touchdevelopUnfiltered -disableGenTest ${j} < ${INPUT_DIRECTORY}/touchdevelop/${i}.model.json \
            | grep . | sed -e "s/^/${i}:/" \
            >> ${OUTPUT_DIRECTORY}/violations-touchdevelop${j}.txt
    done
    echo === Cassandra violations with option ${j}
    rm -f ${OUTPUT_DIRECTORY}/violations-cassandra${j}.txt
    for i in "${cassandra[@]}"
    do
        echo ${i}
        ${CFOUR} -dumpViolationIDs -cassandraUnfiltered -disableGenTest ${j} < ${INPUT_DIRECTORY}/cassandra/${i}-unfiltered.json \
            | grep . | sed -e "s/^/${i}:/" \
            >> ${OUTPUT_DIRECTORY}/violations-cassandra${j}.txt
    done
done

for b in "${backends[@]}"
do
    for o in "${options[@]:1}"
    do
        # quickCheck violations still found with SMT check with option o disabled
        comm -12 \
            <(sort ${OUTPUT_DIRECTORY}/violations-${b}${o}.txt) \
            <(sort ${OUTPUT_DIRECTORY}/violations-${b}-quickCheck.txt) \
            > ${OUTPUT_DIRECTORY}/violations-${b}-baseline${o}.txt
    done
done

for b in "${backends[@]}"
do
    echo "For ${b}, open http://bioinformatics.psb.ugent.be/webtools/Venn/ - upload files:"
    for o in "${options[@]:2}"
    do
        # violations which require o to be enabled to be eliminated by smt check
        comm -23 \
            <(sort ${OUTPUT_DIRECTORY}/violations-${b}-baseline${o}.txt) \
            <(sort ${OUTPUT_DIRECTORY}/violations-${b}-baseline.txt) \
            > ${OUTPUT_DIRECTORY}/violations-${b}-venn${o}.txt
    echo "${OUTPUT_DIRECTORY}/violations-${b}-venn${o}.txt"
    done
done


